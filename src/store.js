import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { logger } from 'redux-logger';
import reducers from './reducers';

// const logger = ReduxLogger();
const store = createStore(reducers, {}, compose(
  applyMiddleware(ReduxThunk, logger)

));

export default store;
