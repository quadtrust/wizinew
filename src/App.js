//    ____                  ____                  __
//   / __ \__  ______ _____/ / /________  _______/ /_
//  / / / / / / / __ `/ __  / __/ ___/ / / / ___/ __/
// / /_/ / /_/ / /_/ / /_/ / /_/ /  / /_/ (__  ) /_
// \___\_\__,_/\__,_/\__,_/\__/_/   \__,_/____/\__/

/* eslint no-global-assign: 0 */
/* eslint no-undef: 0 */
/* eslint no-underscore-dangle: 0 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
// import { StackNavigator } from 'react-navigation';

import store from './store';

import Root from './root';

// To see all the requests in the chrome Dev tools in the network tab.
XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
	GLOBAL.originalXMLHttpRequest :
	GLOBAL.XMLHttpRequest;

  // fetch logger
global._fetch = fetch;
global.fetch = function (uri, options, ...args) {
  return global._fetch(uri, options, ...args).then((response) => {
    console.log('Fetch', { request: { uri, options, ...args }, response });
    return response;
  });
};

class App extends Component {

  render() {
    return (
      <Provider store={store}>
				<Root />
      </Provider>
    );
  }
}

export default App;
