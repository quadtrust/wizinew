//    ____                  ____                  __
//   / __ \__  ______ _____/ / /________  _______/ /_
//  / / / / / / / __ `/ __  / __/ ___/ / / / ___/ __/
// / /_/ / /_/ / /_/ / /_/ / /_/ /  / /_/ (__  ) /_
// \___\_\__,_/\__,_/\__,_/\__/_/   \__,_/____/\__/


import React, { Component } from 'react';
import { AppState, BackHandler, Vibration } from 'react-native';
// import { StackNavigator } from 'react-navigation';

// import Sound from 'react-native-sound';
import { connect } from 'react-redux';


import NavigatorService from './services/navigator';
import { Stacks } from './router';
import Sounds from './services/Sounds';


class Root extends Component {

  state = {
      appState: AppState.currentState
    }

  componentWillMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    // This will load all the sound files.
    Sounds.loadSounds();
  }


    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.props.vibration) {
          Vibration.vibrate([0, 90, 90, 0]);
        }
      });
    }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
      if (this.props.music) {
        Sounds.playBackground();
      }
    } else {
      Sounds.stopBackground();
    }
    this.setState({ appState: nextAppState });
  }


  render() {
    return (
				<Stacks
					ref={navigatorRef => {
					NavigatorService.setContainer(navigatorRef);
				}}
				/>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    music: state.settings.music,
    vibration: state.settings.vibration
  };
};

export default connect(mapStateToProps)(Root);
