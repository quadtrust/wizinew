// import React from 'react';
import {
  StackNavigator
} from 'react-navigation';

import Login from './screens/Login';
import Register from './screens/Register';
import SuggestQuestion from './screens/SuggestQuestion';
import HaveQuestion from './screens/Questions';
import AskQuestion from './screens/AskQuestion';

import NewGame from './screens/NewGame';
import Subject from './screens/Game';
import Results from './screens/ProgressReport';
import Spinner from './screens/Spinner';
import Selected from './screens/Selected';
import SubjectSelection from './screens/SubjectSelection';
import Settings from './screens/Setting';
import Menu from './screens/menu';
import UnlockCharacter from './screens/CharacterUnlocked';
import ChooseCharacter from './screens/ChooseCharacter';
import Social from './screens/Social';
import Statistics from './screens/Statistics';
import Shop from './screens/Shop';
import Help from './screens/Help';
import Language from './screens/ChooseLanguage';
import Profile from './screens/Profile';

export const Stacks = StackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      title: 'Wizi Quiz'
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      title: 'Register'
    }
  },
  NewGame: {
    screen: NewGame,
    navigationOptions: {
      title: 'NewGame',
    }
  },
  Spinner: {
    screen: Spinner
  },
  Selected: {
    screen: Selected
  },
  SubjectSelection: {
    screen: SubjectSelection
  },
  Results: {
    screen: Results
  },
  Menu: {
    screen: Menu
  },
  Subject: {
    screen: Subject,
    navigationOptions: {
      title: 'Subject',
    },
  },
  Settings: {
    screen: Settings
  },
  SuggestQuestion: {
    screen: SuggestQuestion
  },
  HaveQuestion: {
    screen: HaveQuestion
  },
  AskQuestion: {
    screen: AskQuestion
  },
  UnlockCharacter: {
    screen: UnlockCharacter
  },
  ChooseCharacter: {
    screen: ChooseCharacter
  },
  Social: {
    screen: Social
  },
  Statistics: {
    screen: Statistics
  },
  Help: {
    screen: Help
  },
  Shop: {
    screen: Shop
  },
  Language: {
    screen: Language
  },
  Profile: {
    screen: Profile
  },
}, {
  // This will remove the header from the entire application
  headerMode: 'none'
});
