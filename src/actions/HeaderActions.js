import NavigatorService from '../services/navigator';

export const sendtoMenu = () => {
	return (dispatch) => {
		dispatch({
			type: 'NAVIGATE_MENU'
		});
		NavigatorService.navigate('Menu');
	};
};
