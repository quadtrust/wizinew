export const inputUpdate = ({ prop, value }) => {
  return {
    type: 'INPUT_FIELD_UPDATE',
    payload: { prop, value }
  };
};

export const submitRegister = (user, token) => (dispatch) => {
  console.log(user);
  console.log('toke', token);
  fetch(`http://localhost:3000/users?token=${token}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user
    })
  }).then((response) => {
    console.log('user data registered', response);
    dispatch({
      type: 'submitRegisterOn',
      payload: result
    });
  }).catch(() => {
    console.log('some error while resgistering');
  });
};
