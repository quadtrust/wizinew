/* eslint no-undef:0 */
import NavigatorService from '../services/navigator';

export const addFbToken = (token) => (dispatch) => {
  const user = {};
  fetch(`https://graph.facebook.com/v2.5/me?fields=id,email,name&access_token=${token}`)
    .then((response) => response.json().then((data) => {
      console.log(data);
      user.fbId = data.id;
      user.email = data.email;
      user.name = data.name;
      user.fbToken = token;
      dispatch({
        type: 'ADD_FB_TOKEN',
        payload: user
      });
      NavigatorService.navigate('NewGame');
    }))

    .catch(() => {
      console.log('ERROR GETTING DATA FROM FACEBOOK');
      dispatch({
        type: 'LOGIN_USER_FAILED'
      });
    });
    // NavigatorService.navigate('Register');
  };

  export const profilePicUpdate = (obj, pic) => (dispatch) => {
    const player = {};
    player.profile_pic = pic;
    player.player_id = obj.player_id;
    player.username = obj.username;
    fetch(`http://localhost:3000/players/${obj.id}`, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        player
      })
    }).then(() => {
      dispatch({
        type: 'PROFILE_PIC_UPDATE',
        payload: pic
      });
      NavigatorService.navigate('Profile');
    }).catch((error) => {
      console.log('Error', error);
    });
  };

  export const createUser = (name, id) => () => {
    const player = {};
    player.username = name;
    player.profile_pic = 0;
    player.player_id = id;
    fetch('http://localhost:3000/players', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        player
      })
    }).then(() => {
      console.log('SUCEes');
      NavigatorService.navigate('Spinner');
    }).catch((error) => {
      console.log('ERROR', error);
      // NavigatorService.navigate('Spinner');
    });
  };

export const getPlayer = (id) => (dispatch) => {
  fetch(`http://localhost:3000/players?player_id=${id}`)
    .then((response) => response.json().then((data) => {
      dispatch({
        type: 'PLAYER_FETCH',
        payload: data
      });
    }))
    .catch((error) => {
      console.log('Error', error);
    });
};
