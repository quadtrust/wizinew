// This action is for the music switch in the settings screen.
export const musicSetting = () => {
	return (dispatch, getState) => {
		// Get the current state of the music setting.
		const music = getState().settings.music;
		// whatever the music setting is, just flip it.
		console.log('The value that I am getting: ', music);
		dispatch({
			type: 'MUSIC_SETTING',
			payload: !music
		});
	};
};

export const vibrationSetting = () => {
	return (dispatch, getState) => {
		const vibration = getState().settings.vibration;
		dispatch({
			type: 'VIBRATION_SETTING',
			payload: !vibration
		});
	};
};
