/* eslint no-undef:0 */
import NavigatorService from '../services/navigator';

export const questionFetch = (subject) => (dispatch) => {
  fetch(`http://localhost:3000/questions?standard=1&subject=${subject}`)
    .then((response) => response.json().then((data) => {
      console.log(data);
      dispatch({
        type: 'QUESTION_FETCHED',
        payload: data
      });
    // NavigatorService.navigate('Subject');
    }))
    .catch(() => {
      console.log('ERROR GETTING');
    });
  };

export const subjectSelection = (subject) => (dispatch) => {
  dispatch({
    type: 'SUBJECT_UPLOAD',
    payload: subject
  });
  NavigatorService.navigate('Selected');
};

export const reduceSpin = (spinsCount) => {
  return {
    type: 'REDUCE_SPINS',
    payload: spinsCount - 1
  };
};

export const showResult = (correctAnswers, questionsAttend, subject, id) => (dispatch) => {
  const result = {};
  result.correct_questions = correctAnswers;
  result.all_questions = questionsAttend;
  result.subject = subject;
  fetch(`http://localhost:3000/results?player_id=${id}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      result
    })
  }).then((response) => response.json().then((data) => {
      console.log(data);
      dispatch({
        type: 'RESULT_UPLOAD',
        payload: result
      });
      // NavigatorService.navigate('Results');
    // NavigatorService.navigate('Subject');
    }))
    .catch(() => {
      console.log('ERROR GETTING');
    });
};

export const unlockCharacter = (character, id) => (dispatch) => {
  fetch(`http://localhost:3000/unlocked_characters?player_id=${id}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      character
    })
  }).then((response) => response.json().then((data) => {
      console.log(data);
      dispatch({
        type: 'UNLOCK_CHARACTER',
        payload: character
      });
      NavigatorService.navigate('UnlockCharacter');
    }))
    .catch(() => {
      console.log('ERROR GETTING');
    });
};

export const fetchUnlockCharacters = (id) => (dispatch) => {
  fetch(`http://localhost:3000/unlocked_characters?player_id=${id}`, {
    method: 'GET'
  }).then((response) => response.json().then((data) => {
    console.log(data);
    dispatch({
      type: 'FETCH_UNLOCK_CHARACTERS',
      payload: data
    });
  })).catch((error) => {
    console.log(error);
  });
};

export const getResult = (id) => (dispatch) => {
  fetch(`http://localhost:3000/results?player_id=${id}`)
    .then((response) => response.json().then((data) => {
      console.log(data);
      dispatch({
        type: 'RESULT_FETCH',
        payload: data
      });
    }))
    .catch(() => {
      console.log('ERROR GETTING');
    });
};
