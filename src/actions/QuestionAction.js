import NavigatorService from '../services/navigator';

export const addSubjectForQuestion = (subject) => (dispatch) => {
  // const user = {};
    dispatch({
      type: 'addSubjectForQuestion',
      payload: subject
    });
    NavigatorService.navigate('AskQuestion');
  };

  export const submitQuestion = (question) => (dispatch) => {
    fetch('http://localhost:3000/questions', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        question
      })
    }).then((response) => response.json().then((data) => {
        console.log(data);
        dispatch({
          type: 'submitQuestion',
          payload: question
        });
      // NavigatorService.navigate('Subject');
      }))
      .catch(() => {
        console.log('ERROR GETTING');
      });
    NavigatorService.navigate('HaveQuestion');
  };
