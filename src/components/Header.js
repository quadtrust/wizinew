import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { sendtoMenu, getPlayer } from '../actions/';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: require('../assets/images/user.png'),
      username: ''
    };
  }
  componentWillMount() {
    this.props.getPlayer(this.props.userid);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.player !== undefined) {
      this.setState({ username: nextProps.player.username });
    switch (nextProps.player.profile_pic) {
      case 1: {
        this.setState({ avatar: require('../assets/human/human1.png') });

        break;
      }
      case 2: {
        this.setState({ avatar: require('../assets/human/human2.png') });

        break;
      }

      case 3: {
        this.setState({ avatar: require('../assets/human/human3.png') });

        break;
      }

      case 4: {
        this.setState({ avatar: require('../assets/human/human4.png') });

        break;
      }
      case 5: {
        this.setState({ avatar: require('../assets/human/human5.png') });

        break;
      }
      case 6: {
        this.setState({ avatar: require('../assets/human/human6.png') });

        break;
      }
      case 7: {
        this.setState({ avatar: require('../assets/human/human7.png') });

        break;
      }
      case 8: {
        this.setState({ avatar: require('../assets/human/human8.png') });

        break;
      }
      case 9: {
        this.setState({ avatar: require('../assets/human/human9.png') });

        break;
      }
      case 10: {
        this.setState({ avatar: require('../assets/human/human10.png') });

        break;
      }
      case 11: {
        this.setState({ avatar: require('../assets/human/human11.png') });

        break;
      }
      case 12: {
        this.setState({ avatar: require('../assets/human/human12.png') });

        break;
      }
      default: {
        this.setState({ avatar: require('../assets/images/user.png') });
      }
    }
    }
  }

  backIconrender() {
    if (this.props.back === 'back') {
      return (
        <TouchableOpacity>
          <Icon name="angle-left" size={50} color="#fff" style={[styles.iconStyle]} />
        </TouchableOpacity>
      );
    }
      return (
        <TouchableOpacity onPress={() => { this.props.sendtoMenu(); }}>
          <Icon name="home" size={30} color="#fff" style={[styles.iconStyle]} />
        </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.headerStyle}>
        <View style={[styles.flexone]} >
          {this.backIconrender()}
        </View>
        <Text style={[styles.textStyle]}>
          {this.props.title}
        </Text>
        <TouchableOpacity
          style={styles.profileContainer}
        >
          <Image source={this.state.avatar} style={styles.profile} />
          <Text style={[styles.playerName, styles.flexone]}>
            {this.state.username}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  flexone: {
    flex: 1,
  },
  profileContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingRight: 15
  },
  profile: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    borderWidth: 1,
    borderColor: 'white'
  },
  iconStyle: {
    paddingLeft: 30
  },

  headerStyle: {
    height: 60,
    backgroundColor: '#403c3d',
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyle: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'mikadoultra',
    flex: 3
  },
  playerName: {
    color: '#fff',
    fontSize: 10,
    textAlign: 'center',
    fontFamily: 'mikadoultra'
  }
};

const mapStateToProps = (state) => ({
  userid: state.auth.user.fbId,
  player: state.auth.player,
});
export default connect(mapStateToProps, { sendtoMenu, getPlayer })(Header);
