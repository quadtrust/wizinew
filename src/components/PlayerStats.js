import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

export default class PlayerStats extends Component {
  constructor(props) {
      super(props);
      this.state = {
          lives_left: '10',
          trophy_left: '10',
      };
  }
    render() {
      return (
        <View style={[styles.base, styles.row]}>
          <View style={{ flex: 1 }} />
            <View style={[styles.row, styles.fullWidth]}>
              <Image
                  style={[styles.unknown, styles.fullWidth]}
                  source={require('../assets/images/common/hat.png')}
              />
              <Text style={[styles.blackColor, styles.fullWidth]}>{this.state.lives_left}</Text>
            </View>
            <View style={[styles.row, styles.fullWidth]}>
              <Image
                  style={[styles.unknown, styles.fullWidth]}
                  source={require('../assets/images/common/trophy.png')}
              />
              <Text
                style={[styles.blackColor, styles.fullWidth]}
              >{this.state.trophy_left}</Text>
            </View>
            <View style={[styles.row, styles.fullWidth]}>
                <Image
                    style={[styles.unknown, styles.fullWidth]}
                    source={require('../assets/images/common/spin.png')}
                />
                <Text style={[styles.blackColor, styles.fullWidth]}>{this.props.spins}</Text>
            </View>
            <View style={[styles.row, styles.fullWidth]}>
                <Image
                    style={[styles.unknown, styles.fullWidth]}
                    source={require('../assets/images/common/heart.png')}
                />
                <Text style={[styles.blackColor, styles.fullWidth]}>{this.props.life}</Text>
            </View>
            <View style={{ flex: 1 }} />
        </View>
      );
    }
}

const styles = StyleSheet.create({
    base: {
      height: 30,
      borderBottomWidth: 1,
      borderBottomColor: '#6e6e6e',
      backgroundColor: 'white'
    },
    row: {
      flexDirection: 'row'
    },
    fullWidth: {
      flex: 1
    },
    unknown: {
      height: undefined,
      width: undefined,
      resizeMode: 'contain',
      margin: 3
    },
    blackColor: {
      color: 'black',
      textAlign: 'left',
      alignSelf: 'center',
      fontWeight: 'bold'
    }
});
