import React, { Component } from 'react';
import {
    ListView,
    View,
    Text
} from 'react-native';

export default class CheckStatus extends Component {

    constructor(props) {
        super(props);

        this.state = {
            questions: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        };
    }

    getCategory(id) {
        switch(id) {
            case 1:
                return 'English';
                break;

            case 2:
                return 'Maths';
                break;

            case 3:
                return 'Islamic Studies';
                break;

            case 4:
                return 'Arabic';
                break;

            case 5:
                return 'Science';
                break;

            case 6:
                return 'Social Studies';
                break;
        }
    }

    componentDidMount() {
        // API.checkQuestionStatus(this.props.doResetTo, (response) => {
        //     if(response.questions) {
                this.setState({
                    questions: this.state.questions
                });
        //     }
        //     this.props.loaded();
        // });
    }

    question(item) {
        return (<View style={{ backgroundColor: '#454344', padding: 20, marginBottom: 10 }}>
            <Text style={styles.normal}>Q: {item.question}</Text>
            <Text style={styles.normal}>Category: {this.getCategory(item.CategoryId)}</Text>
            <Text style={styles.normal}>Status: {item.active ? 'Approved' : 'Unapproved'}</Text>
        </View>);
    }

    render() {
        return (<View style={{ flex: 1, backgroundColor: '#2B292A' }}>
            {/* <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/> */}
            <View style={{ flex: 18 }}>
                <Text
                style={{ fontSize: 25,
                   marginLeft: 20,
                    marginRight: 20,
                     color: 'orange',
                      fontWeight: '800',
                       textAlign: 'center',
                        marginTop: 20 }}
                >Question Status</Text>
                <ListView
                    style={{ marginTop: 20, marginHorizontal: 30 }}
                    dataSource={this.state.questions}
                    renderRow={(item) => this.question(item)}
                />
            </View>
        </View>);
    }

}

const styles = {
    normal: {
        color: 'white',
        fontSize: 16
    }
};
