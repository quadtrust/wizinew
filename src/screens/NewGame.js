import React, { Component } from 'react';
import { View,
    Text,
    // Image,
    // ScrollView,
    // TextInput,
    StyleSheet,
    TouchableOpacity } from 'react-native';

  import { connect } from 'react-redux';
  import { createUser } from '../actions/';


class NewGame extends Component {
  constructor(props) {
    super(props);
    this.changeMode = this.changeChallangeMode.bind(this);
    this.changeMode = this.changeClasssicMode.bind(this);
    this.changeMode = this.changeFriendsMode.bind(this);
    this.changeMode = this.changeRandomMode.bind(this);
    this.changeMode = this.changeDirectorateMode.bind(this);
    this.state = {
      challengeMode: false,
      classsicMode: false,
      friendsMode: false,
      randomMode: false,
      directorateMode: false
    };
  }
  // componentWillUnmount() {
  //   if (this.props.username !== null) {
  //     this.props.createUser(this.props.username);
  //   }
  // }
  _onPress() {
    if (this.props.username !== null) {
      this.props.createUser(this.props.username, this.props.userid);
    }
  }
  changeChallangeMode() {
    this.setState({ challengeMode: true, classsicMode: false });
  }
  changeClasssicMode() {
    this.setState({ classsicMode: true,
       challengeMode: false,
        friendsMode: false,
         directorateMode: false,
          randomMode: false });
  }
  changeFriendsMode() {
    this.setState({ friendsMode: !this.state.friendsMode,
       randomMode: false,
        directorateMode: false });
  }
  changeRandomMode() {
    this.setState({ randomMode: !this.state.randomMode,
       friendsMode: false,
        directorateMode: false });
  }
  changeDirectorateMode() {
    this.setState({ directorateMode: !this.state.directorateMode,
    friendsMode: false,
    randomMode: false });
  }
  render() {
  let challengeMode = <View />;

  if (this.state.challengeMode === true) {
    challengeMode = (<View>
      <Text
      style={[styles.center,
         { fontSize: 22, fontFamily: 'mikadoultra', color: '#000', marginTop: 20 }]}
      >
        CHOOSE YOUR OPPONENT
        </Text>
      <View style={[styles.row, { justifyContent: 'space-around' }]}>
        <View
        style={[this.state.friendsMode ?
                    styles.activeBackFriends : styles.inActiveBackFriends, {
          justifyContent: 'center',
          alignItems: 'center',
          height: 36
        }]}
        >
      <TouchableOpacity
          onPress={() => this.changeFriendsMode()}
      >
        <Text
          style={[{ fontSize: 18, color: 'white', fontWeight: 'bold' }]}
        >FRIENDS</Text></TouchableOpacity>
        </View>
        <View
        style={[this.state.randomMode ? styles.activeBackRandom : styles.inActiveBackRandom, {
          justifyContent: 'center',
          alignItems: 'center',
          height: 36
        }]}
        >
        <TouchableOpacity

          onPress={() => this.changeRandomMode()}

        >
        <Text
          style={[{ fontSize: 18, color: 'white', fontWeight: 'bold' }]}
        >RANDOM</Text></TouchableOpacity>

        </View>
      </View>
      <View style={{ alignItems: 'center' }}>
        <View
        style={[this.state.directorateMode ?
          styles.activeBackFriends : styles.inActiveBackFriends, {
          justifyContent: 'center',
          alignItems: 'center',
          height: 36
        }]}
        >
        <TouchableOpacity
            onPress={() => this.changeDirectorateMode()}
        >
        <Text
        style={[{ fontSize: 18, color: 'white', fontWeight: 'bold' }]}

        >DIRECTORATE</Text></TouchableOpacity>
        </View>
      </View>
    </View>);
  }

  return (
    <View style={[styles.fullWidth]}>
      {/* <Header doLoad={this.props.doLoad} doPop={this.props.doPop} player={true} /> */}
      <View style={[styles.bodyWidth, styles.whiteBack]}>
        <View style={{ backgroundColor: '#F06C50' }}>
          <Text style={[styles.white, styles.challenge_header, styles.center]}>NEW GAME</Text>
        </View>
        <Text
            style={[styles.center,
           {
             fontFamily: 'mikadoultra',
             fontSize: 22,
            //  fontWeight: '600',
             color: '#000',
             marginTop: 20
            //  fontFamily: 'mikadoultra'
           }]}
        >
          SELECT YOUR GAME MODE
            </Text>
        <View style={[styles.row, { justifyContent: 'space-around' }]}>
          <View
          style={[
            styles.classicActive, {
            justifyContent: 'center',
            alignItems: 'center',
          }]}
          >
          <TouchableOpacity
            onPress={() => this.changeClasssicMode()}
          >
          <Text
          style={[this.state.classsicMode ? styles.activeClassic : styles.inactiveClassic, {
            fontWeight: 'bold',
            width: 150,
            textAlign: 'center',
            padding: 4
          }]}
          >  CLASSIC
            </Text>
            </TouchableOpacity>
          </View>
          <View
            style={[
              styles.challengeActive, {
              justifyContent: 'center',
              alignItems: 'center',
            }]}
          >

          <TouchableOpacity
            onPress={() => this.changeChallangeMode()}
          >
          <Text
          style={[this.state.challengeMode ? styles.activeChallenge : styles.inactiveClassic, {
            fontWeight: 'bold',
            padding: 4,
            width: 150,
            textAlign: 'center'
          }]}
          >
          CHALLENGE
          </Text></TouchableOpacity>
          </View>

        </View>
        {challengeMode}
        <Text
        style={[styles.center,
           { color: '#000', marginTop: 10 }]}
        >Challenge mode allows you to play against several challengers
        </Text>

        <View style={[styles.row, { justifyContent: 'center', marginTop: 30 }]}>
          <View
          style={[{
            marginTop: 20,
            padding: 5,
            width: 230,
            borderRadius: 5,
            backgroundColor: '#1EBBA7',
            justifyContent: 'center',
            alignItems: 'center',
            height: 36
          }]}
          >
          <TouchableOpacity
            onPress={() => this._onPress()}
          >
          <Text
          style={[{ fontSize: 18, fontWeight: 'bold', color: 'white' }]}
          >PLAY NOW</Text>
          </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontWeight: '700',
        fontSize: 23,
        color: '#00AEFF',

    },
    selected_header: {
        fontWeight: '700',
        fontSize: 25,
        color: '#00AEFF',

    },
    challenge_header: {
        fontFamily: 'mikadoultra',
        fontSize: 35,
        color: '#ffffff',

    },
    challenge_sub_header: {
        fontWeight: '100',
        fontSize: 23,
        color: '#ffffff',

    },
    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 4,
        resizeMode: 'stretch',
        marginTop: 20,
        marginHorizontal: 20
    },
    whiteBack: {
        backgroundColor: '#FEFFFF'
    },
    center_element: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    languageHeader: { fontWeight: '700', fontSize: 23, color: 'white', textAlign: 'center' },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    challengeButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackFriends: {
      backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    activeBackRandom: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackRandom: {
        backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inactiveClassic: {
        fontSize: 18, color: 'white'
    },
    activeClassic: {
        fontSize: 18, color: 'white', borderColor: '#F11F43', borderBottomWidth: 4
    },

    activeChallenge: {
        fontSize: 18, color: 'white', borderColor: '#00C0FF', borderBottomWidth: 4
    },

    challengeActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#F11F43',
        width: 150
    },
    classicActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#00C0FF',
        width: 150
    },
    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },
    competitionText: {
        color: '#139BFF',
        fontWeight: '900',
        fontSize: 30,

    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        marginTop: 10,
        position: 'absolute',
        zIndex: 1000
    },
    nextText: {
        fontWeight: '800', fontSize: 20, color: '#fff'
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15,
        paddingTop: 2
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        paddingHorizontal: 35,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },

    textCenter: {
        textAlign: 'center'
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 30,
        textDecorationLine: 'underline',
    },

});

const mapStateToProps = (state) => ({
  username: state.auth.user.name,
  userid: state.auth.user.fbId
});

export default connect(mapStateToProps, { createUser })(NewGame);
