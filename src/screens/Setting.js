import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import { CheckboxField } from 'react-native-checkbox-field';
import { connect } from 'react-redux';

import Header from '../components/Header';
import Sounds from '../services/Sounds';

import { musicSetting, vibrationSetting } from '../actions/';

class Setting extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sound: this.props.musicSettingValue,
      vibration: this.props.vibration,
      notifications: false,
      notification_sounds: false,
      notification_vibration: false
    };
    // this.selectCheckbox = this.selectCheckbox.bind(this);
  }

  componentDidMount() {
    console.log('The current value of the music setting is :', this.props.musicSettingValue);
  }

    selectCheckbox(id) {
      switch (id) {
          case 1:
              this.setState({ sound: !this.state.sound });
              this.props.musicSetting(this.state.sound);
              if (this.state.sound) {
                Sounds.stopBackground();
              } else {
                Sounds.playBackground();
              }
              break;
          case 2:
              this.setState({ vibration: !this.state.vibration });
              this.props.vibrationSetting();
              break;
          case 3:
              this.setState({ notifications: !this.state.notifications });
              break;
          case 4:
              this.setState({ notification_sounds: !this.state.notification_sounds });
              break;
          case 5:
              this.setState({ notification_vibration: !this.state.notification_vibration });
              break;
          default:
              return null;
        }
    }

    render() {
      const defaultColor = '#fff';
      return (
        <View style={styles.fullWidth}>
          <Header title={'SETTINGS'} back={'back'} />
          <View style={styles.bodyWidth}>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[styles.settingText]}>
                SETTINGS
              </Text>
            </View>
            <View style={[styles.checkContainer]}>
              <TouchableOpacity>
                <CheckboxField
                  onSelect={() => {
                    this.selectCheckbox(1);
                  }}
                  selected={this.state.sound}
                  defaultColor={defaultColor}
                  selectedColor="#61C6B4"
                  checkboxStyle={styles.checkboxStyle}
                >
                  <Text style={{ color: defaultColor }}>✓</Text>
                </CheckboxField>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.selectCheckbox(1); }}>
                <Text style={[styles.checkText]}>
                  SOUND
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <CheckboxField
                onSelect={() => {
                  this.selectCheckbox(2);
                }}
                selected={this.state.vibration}
                defaultColor={defaultColor}
                selectedColor="#61C6B4"
                checkboxStyle={styles.checkboxStyle}
              >
                <Text style={{ color: defaultColor }}>✓</Text>
              </CheckboxField>
              <TouchableOpacity onPress={() => { this.selectCheckbox(2); }}>
                <Text style={[styles.checkText]}>
                  VIBRATION
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <CheckboxField
                onSelect={() => {
                  this.selectCheckbox(3);
                }}
                selected={this.state.notifications}
                defaultColor={defaultColor}
                selectedColor="#61C6B4"
                checkboxStyle={styles.checkboxStyle}
              >
                <Text style={{ color: defaultColor }}>✓</Text>
              </CheckboxField>
              <TouchableOpacity onPress={() => { this.selectCheckbox(3); }}>
                <Text style={[styles.checkText]}>
                  NOTIFICATIONS
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <CheckboxField
                onSelect={() => {
                  this.selectCheckbox(4);
                }}
                selected={this.state.notification_sounds}
                defaultColor={defaultColor}
                selectedColor="#61C6B4"
                checkboxStyle={styles.checkboxStyle}
              >
                <Text style={{ color: defaultColor }}>✓</Text>
              </CheckboxField>
              <TouchableOpacity onPress={() => { this.selectCheckbox(4); }}>
                <Text style={[styles.checkText]}>
                  NOTIFICATION SOUND
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <CheckboxField
                onSelect={() => {
                  this.selectCheckbox(5);
                }}
                selected={this.state.notification_vibration}
                defaultColor={defaultColor}
                selectedColor="#61C6B4"
                checkboxStyle={styles.checkboxStyle}
              >
                <Text style={{ color: defaultColor }}>✓</Text>
              </CheckboxField>
              <TouchableOpacity onPress={() => { this.selectCheckbox(5); }}>
                <Text style={[styles.checkText]}>
                  NOTIFICATION VIBRATION
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('ChooseCharacter'); }}>
                <Text style={[styles.checkText]}>
                    CHANGE AVATAR
                 </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.checkContainer]}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Language')}>
                <Text style={[styles.checkText]}>
                    CHANGE LANGUAGE
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.center, { flexDirection: 'row' }]}>
              <View
                style={[{ backgroundColor: '#F34F4B' },
                styles.button
              ]}
              >
                <TouchableOpacity>
                  <Text style={[styles.textCenter, styles.buttonText]}>LOG OUT</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
      flex: 1
    },
    settingText: {
      fontSize: 30,
      color: '#4B82C3',
      fontFamily: 'mikadoultra',
      marginTop: 25
     },
    checkContainer: {
      borderBottomWidth: 2,
      borderColor: '#D1D2D2',
      paddingBottom: 8,
      marginTop: 8,
      flexDirection: 'row',
      alignItems: 'center'
    },
    center: {
      alignItems: 'center',
      justifyContent: 'center'
    },
    textCenter: {
      textAlign: 'center'
    },
    buttonText: {
      color: 'white',
      fontFamily: 'mikadoultra',
      fontSize: 16
    },
    checkText: {
      color: '#000',
      fontSize: 20,
      fontFamily: 'mikadoultra',
      paddingLeft: 15,
      borderLeftWidth: 1,
      borderColor: '#D1D2D2'
    },

    bodyWidth: {
      flex: 18,
      backgroundColor: '#fff'
    },
    containerStyle: {
      padding: 20,
    },
    labelStyle: {
      flex: 1
    },
    checkboxStyle: {
      width: 36,
      height: 36,
      borderWidth: 2,
      borderColor: '#61C6B4',
      borderRadius: 1
    },
    button: {
      width: 160,
      height: 40,
      marginTop: 20,
      borderRadius: 5,
      marginLeft: 5,
      alignItems: 'center',
      justifyContent: 'center'
    }
});

const mapStateToProps = (state) => {
  return {
    musicSettingValue: state.settings.music,
    vibration: state.settings.vibration
  };
};

export default connect(mapStateToProps, { musicSetting, vibrationSetting })(Setting);
