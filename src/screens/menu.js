import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet
} from 'react-native';
import Header from '../components/Header';

export default class Menu extends Component {

  componentDidMount() {
  }

  render() {
    return (
      <View style={[styles.fullWidth]}>
        {/*background*/}
        <Image
          style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
          source={require('../assets/images/menu/back.png')}
        >

          <Header title={'Menu'} back={'back'} />

          <View style={[styles.center, styles.transparenWhite, { flex: 2, marginBottom: 10 }]}>
            <Image
              source={require('../assets/images/menu/menuCopy.png')}
              style={[{ resizeMode: 'contain', width: 350, height: 100 }]}
            />
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')} >
              <Image
                source={require('../assets/images/menu/profile.png')}
                style={[{ resizeMode: 'contain', width: 200, height: 22 }]}
              />
            </TouchableOpacity>
          </View>

          <View style={[{ flex: 7, justifyContent: 'center', marginBottom: 10 }]}>

            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.navigate('NewGame')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> HOME </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
            onPress={() => this.props.navigation.navigate('Results')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}> REPORT </Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.navigate('Statistics')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
              <Text style={[styles.textCenter, styles.menuContainerText]}>STATISTICS</Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
             onPress={() => this.props.navigation.navigate('HaveQuestion')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
              <Text style={[styles.textCenter, styles.menuContainerText]}>QUESTION BANK</Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
             onPress={() => this.props.navigation.navigate('Social')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}>SOCIAL MEDIA</Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
             onPress={() => this.props.navigation.navigate('Shop')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}>SHOP</Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
             onPress={() => {
               this.props.navigation.navigate('Settings');
               console.log('this.props.navigation.navigate(Settings)');
            }}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}>SETTING</Text>
              </Image>
            </TouchableOpacity>

            <TouchableOpacity
             style={{ flex: 1 }}
              onPress={() => this.props.navigation.navigate('Help')}
            >
              <Image
                source={require('../assets/images/menu/slot.jpg')}
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
              >
                <Text style={[styles.textCenter, styles.menuContainerText]}>HELP</Text>
              </Image>
            </TouchableOpacity>

          </View>
        </Image>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1
  },
  menuContainer: { height: 55, width: 200 },
  menuContainerText: {
    fontFamily: 'mikadoultra',
    fontSize: 16,
    color: '#fff',
    padding: 10
  },
  contain: {
    resizeMode: 'contain'
  },
  transparenWhite: {
    backgroundColor: 'rgba(255,255,255,0.6)',
    paddingBottom: 10

  },
  notKnown: {
    height: undefined,
    width: undefined
  },
  zoomed: {
    resizeMode: 'cover'
  },
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  textCenter: {
    textAlign: 'center'
  },
  menuText: {
    color: '#fff',
    fontWeight: '800',
    fontSize: 40
  },
  height: {
    height: 30
  },
  blackColor: {
    backgroundColor: '#403c3d'
  },

});
