import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

const style = {
    heading: {
        fontSize: 18,
        color: '#c41220',
        fontWeight: 'bold'
    },
    listRight: {
        fontSize: 18,
        color: '#00aeff',
        fontWeight: 'bold',
        flex: 1,
        textAlign: 'right'
    },
    listLeft: {
        fontSize: 18,
        color: '#00aeff',
        fontWeight: 'bold',
        flex: 1,
        textAlign: 'left'
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    mapImage: {
      flex: 1,
      height: undefined,
      width: undefined
    },
    headingText: {
      color: '#00aeff',
      fontSize: 30,
      fontWeight: 'bold',
      textAlign: 'center'
    }
};

export default class Directorates extends Component {
  constructor(props) {
    super(props);
    this.state = {
        directorates: []
    };
  }

    // componentDidMount() {
    //   API.getDirectorates((directorates) => {
    //       if (directorates.error) {
    //
    //       } else {
    //           this.setState({
    //               directorates: directorates.directorates
    //           });
    //       }
    //   });
    // }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 18, padding: 10 }}>
                    <Text
                        style={style.headingText}
                    >COMPETITION</Text>
                    <Image
                        style={style.mapImage}
                        resizeMode='contain'
                        source={require('../assets/images/directorates/map.png')}
                    />

                    <View>
                        <View
                          style={[{ flexDirection: 'row', justifyContent: 'space-between' },
                          style.borderBottom]}
                        >
                            <Text style={style.heading}>GOVERNORATE</Text>
                            <Text style={style.heading}>PLAYERS</Text>
                            <Text style={style.heading}>SCORES</Text>
                        </View>
                        <TouchableOpacity
                      //     onPress={() => {
                      //       this.props.doLoad('topDirectoratePlayers', { id: directorate.id })
                      //   }
                      // }
                        style={[{ flexDirection: 'row', justifyContent: 'space-between' },
                          style.borderBottom]}
                        >
                            <Text style={style.listLeft}>name</Text>
                            <Text style={style.listRight}>user</Text>
                            <Text style={style.listRight}>total</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}
