import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import GridView from 'react-native-easy-grid-view';
import { connect } from 'react-redux';
import Header from '../components/Header';
import { profilePicUpdate, fetchUnlockCharacters } from '../actions';
// import API from './API';
// import Sockets from './Sockets';

class ChooseCharacter extends Component {
    constructor(props) {
        super(props);
        const ds = new GridView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.state = {
          clickedId: 0,
            avatars: ds.cloneWithCells([
              {
                characterId: '1',
                characterSource: require('../assets/human/human1.png')
              },
              {
                characterId: '2',
                characterSource: require('../assets/human/human2.png')
              },
              {
                characterId: '3',
                characterSource: require('../assets/human/human3.png')
              },
              {
                characterId: '4',
                characterSource: require('../assets/human/human4.png')
              },
              {
                characterId: '5',
                characterSource: require('../assets/human/human5.png')
              },
              {
                characterId: '6',
                characterSource: require('../assets/human/human6.png')
              },
              {
                characterId: '7',
                characterSource: require('../assets/human/human7.png')
              },
              {
                characterId: '8',
                characterSource: require('../assets/human/human8.png')
              },
              {
                characterId: '9',
                characterSource: require('../assets/human/human9.png')
              },
              {
                characterId: '10',
                characterSource: require('../assets/human/human10.png')
              },
              {
                characterId: '11',
                characterSource: require('../assets/human/human11.png')
              },
              {
                characterId: '12',
                characterSource: require('../assets/human/human12.png')
              },
            ], 4),
            cellWidth: 0,
            cellHeight: 0,
            // unlocked: [],
            // myAvatar: null
        };
    }

    componentWillMount() {
      this.props.fetchUnlockCharacters(this.props.player_id);
      // console.log('Player', this.props.player_id);
      // console.log('Player...', this.props.player);
    }

    chooseChar(cell) {
      console.log('Player', this.props.player.id);
      this.setState({ clickedId: cell.characterId });
      this.props.profilePicUpdate(this.props.player, cell.characterId);
    }

    avatarCell(cell) {
      return (<View >
            <TouchableOpacity
              onPress={() => this.chooseChar(cell)}
              style={this.state.clickedId === cell.characterId ? styles.clickedAvatar : styles.unClickedAvatar}
              disabled={!(this.props.unlockcharacterlist.indexOf(cell.characterId) > -1)}
            >
                <Image
                    source={cell.characterSource}
                    style={{
                    flex: 1,
                    height: 150,
                    width: undefined,
                    opacity: (this.props.unlockcharacterlist.indexOf(cell.characterId) > -1 ? 1 : 0.2)
                    }}
                    resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View>
      );
    }

    render() {
        return (
            <View style={styles.fullWidth}>
                <Header title={'WIZI QUIZ'} />
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text style={[styles.avatar_header, styles.center]}>CHOOSE YOUR AVATAR</Text>
                    <GridView
                        dataSource={this.state.avatars}
                        renderCell={this.avatarCell.bind(this)}
                    />

                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    subjectContainer: {
        flex: 1,
    },
    questionsAsked: {
        flex: 1,
        alignItems: 'center',
    },
    clickedAvatar: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 5,
        padding: 5
    },
    unClickedAvatar: {
      backgroundColor: 'transparent'
    },
    fullWidth: {
        flex: 1,
        backgroundColor: 'white'
    },
    nextText: {
        fontWeight: '800',
        fontSize: 16,
        color: '#fff',
      },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontFamily: 'mikadoultra',
        fontSize: 23,
        color: '#00AEFF',
        paddingVertical: 15
    },

    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },

    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },

    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },

    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },

    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },

    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },

    textCenter: {
        textAlign: 'center'
    },

    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 20,
        textDecorationLine: 'underline',
    },

});

const mapStateToProps = (state) => ({
  unlockcharacterlist: state.gamequestion.unlockCharacterList,
  profilepic: state.auth.profilepic,
  player: state.auth.player,
  player_id: state.auth.player.player_id
});
export default connect(mapStateToProps, { profilePicUpdate, fetchUnlockCharacters })(ChooseCharacter);
