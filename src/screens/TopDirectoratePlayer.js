import React, { Component } from 'react';
import {
    View,
    Text,
    ToastAndroid,
    TouchableOpacity,
    NativeModules,
    AsyncStorage
} from 'react-native';

const style = {
    heading: {
        fontSize: 18,
        color: '#c41220',
        fontWeight: 'bold'
    },
    list: {
        fontSize: 18,
        color: '#00aeff',
        fontWeight: 'bold'
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    }
};

export default class TopDirectoratePlayers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            players: [
               {
                first_name: 'Mritunjay',
                last_name: 'Upadhyay',
                marks: 30
              },
             {
                first_name: 'Mritunjay',
                last_name: 'Upadhyay',
                marks: 30
              },
               {
                first_name: 'Mritunjay',
                last_name: 'Upadhyay',
                marks: 30
              }
            ],
            user: {}
        };
    }

    componentDidMount() {
        // API.getTopDirectoratePlayers(this.props.data.id, this.props.doResetTo, (response) => {
        //     // if(response.error) {
        //     //     ToastAndroid.show('There was some error, please try again in sometime', ToastAndroid.SHORT);
        //     //     this.props.doPop();
        //     // } else {
        //         this.setState({
        //             players: response.users
        //         });
        //     // }
        // });
        //
        // Sockets.on('invite-result', (data) => {
        //     if (data[0].result) {
        //         this.props.doReplace('challenge');
        //     } else {
        //         ToastAndroid.show('This username does not exist', ToastAndroid.SHORT);
        //     }
        // });
        //
        // AsyncStorage.getItem('user').then((resp) => {
        //     this.setState({
        //         user: JSON.parse(resp)
        //     });
        // });
    }

    componentWillUnmount() {
        // NativeModules.SocketIO.off('invite-result');
    }

    sendInvite(username, name) {
        // if(username !== this.state.user.user.username){
        //     Sockets.emit('send-invite', { username: username });
        //     ToastAndroid.show('Invite sent to ' + name, ToastAndroid.SHORT);
        // } else {
        //     ToastAndroid.show('You cannot invite yourself', ToastAndroid.SHORT);
        // }
    }

    renderRowPlayer(player) {
        return (
            <TouchableOpacity
            //  onPress={() =>
            //    this.sendInvite(player.username, player.first_name + " " + player.last_name)}
            style={[{ flexDirection: 'row', justifyContent: 'space-between' }, style.borderBottom]}
            >
                <Text style={style.list}>{player.first_name} {player.last_name}</Text>
                <Text style={style.list}>{player.marks}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
              {/* <Header
                style={{flex: 1}} doLoad={this.props.doLoad} doPop={this.props.doPop} />*/}
                <View style={{ flex: 18, padding: 10 }}>
                    <View
                    style={[{ flexDirection: 'row',
                     justifyContent: 'space-between' }, style.borderBottom]}
                    >
                        <Text style={style.heading}>PLAYER</Text>
                        <Text style={style.heading}>SCORES</Text>
                    </View>
                    {this.state.players.map((player) => this.renderRowPlayer(player))}
                </View>
            </View>
        );
    }

}
