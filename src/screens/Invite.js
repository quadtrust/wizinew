import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity
} from 'react-native';

export default class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: ''
    };
  }

  focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

  render() {
    return (
      <View style={styles.head}>
        <View style={[styles.body]}>
          <Text
            style={[styles.blueFont,
              styles.textCenter,
              styles.bold,
              styles.h1,
              styles.mt30, styles.custom]}
          > INVITE YOUR FRIENDS </Text>
          <Text
            style={[styles.textCenter,
            styles.bold,
            styles.black,
            styles.h3,
            styles.custom]}
          > EACH INVITE WILL EARN YOU 10 WIZI COINS</Text>
          <Text
            style={[styles.textCenter,
              styles.padVertical,
              styles.black,
              styles.mediumFont,
              styles.bold,
              styles.custom]}
          >YOUR FRIENDS NAME</Text>
          <TextInput
            style={[styles.field, styles.black, styles.margins, styles.someMarginTop]}
            placeholder=""
            underlineColorAndroid="transparent"
            returnKeyType="next"
            onChangeText={name => this.setState({ name })}
            value={this.state.name}
            onSubmitEditing={() => this.focusNextField('email')}
          />
          <Text
            style={[styles.textCenter,
              styles.black,
              styles.mediumFont,
              styles.padVertical,
              styles.bold]}
          >YOUR FRIENDS EMAIL ADDRESS</Text>
          <TextInput
            ref="email"
            style={[styles.field, styles.black, styles.margins, styles.someMarginTop]}
            placeholder=""
            underlineColorAndroid="transparent"
            returnKeyType="done"
            onChangeText={email => this.setState({ email })}
            keyboardType="email-address"
            value={this.state.email}
          />
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.buttonStyle}>
              <TouchableOpacity>
                <Text style={[styles.textCenter, { color: 'white', fontWeight: 'bold' }]}>
                  INVITE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Image
            style={{ resizeMode: 'contain', height: 200, width: 100 }}
            source={require('../assets/images/invite/logo-half.png')}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  head: {
    flex: 1,
    backgroundColor: 'white'
  },
  custom: {
    fontFamily: 'sans-serif'
  },
  body: {
    flex: 18
  },
  padVertical: {
    paddingTop: 20,
    paddingBottom: 20
  },
  h1: {
    fontSize: 30
  },
  mediumFont: {
    fontSize: 20
  },
  h3: {
    fontSize: 13
  },
  blueFont: {
    color: '#209CB6'
  },
  textCenter: {
    textAlign: 'center'
  },
  bold: {
    fontWeight: '800'
  },
  black: {
    color: '#000'
  },
  field: {
    height: 50,
    backgroundColor: '#EBEBEB',
    borderColor: '#ACABAB',
    borderWidth: 1,
    padding: 2
  },
  red: {
    backgroundColor: '#f44f4b',
  },
  mt30: {
    marginTop: 30
  },
  buttonStyle: {
    backgroundColor: '#F34F4B',
    width: 130,
    height: 30,
    marginTop: 20,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
},
  someMarginTop: {
    marginTop: 5
  },
  margins: {
    marginLeft: 50,
    marginRight: 50
  },
});
