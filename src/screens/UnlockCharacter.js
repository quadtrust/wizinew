import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ListView,
    StyleSheet
} from 'react-native';
import GridView from 'react-native-easy-gridview';

export default class UnlockCharacter extends Component {
    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.state = {
            avatars: ds.cloneWithRows([]),
            unlocked: false
        }
    }

    componentDidMount() {

    }

    chooseChar(char) {
        console.log('heoo', char);
    }

    avatarRow(a) {
        return (
          <View style={{ margin: 5 }}>
                <TouchableOpacity
                  disabled={(this.state.unlocked !== false
                  ? (this.state.unlocked.indexOf(a + 1) <= -1)
                  : true)}
                  onPress={() => this.chooseChar(a + 1)}
                >
                <Image
                    source={require('../assets/images/common/english-character.png')}
                    style={{
                        flex: 1,
                        height: 100,
                        width: undefined,
                        opacity: (this.state.unlocked !== false ?
                           (this.state.unlocked.indexOf(a + 1) > -1 ? 1 : 0.2) : 0.2)
                             }}
                    resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View>
      );
    }

    render() {
        return (
            <View style={styles.fullWidth}>
            { /* <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true}/>*/}
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text style={[styles.avatar_header, styles.center]}>UNLOCK AN AVATAR</Text>
                    <GridView
                        dataSource={this.state.avatars}
                        renderRow={(item) => this.avatarRow(item)}
                        numberOfItemsPerRow={4}
                        removeClippedSubviews={false}
                        initialListSize={1}
                        pageSize={5}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontWeight: '700',
        fontSize: 23,
        color: '#00AEFF',

    },
    selected_header: {
        fontWeight: '700',
        fontSize: 25,
        color: '#00AEFF',

    },
    challenge_header: {
        fontWeight: '900',
        fontSize: 35,
        color: '#ffffff',

    },
    challenge_sub_header: {
        fontWeight: '100',
        fontSize: 23,
        color: '#ffffff',

    },
    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 4,
        resizeMode: 'stretch',
        marginTop: 20,
        marginHorizontal: 20
    },
    whiteBack: {
        backgroundColor: '#FEFFFF'
    },
    center_element: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    languageHeader: { fontWeight: '700', fontSize: 23, color: 'white', textAlign: 'center' },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    challengeButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackFriends: {
      backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    activeBackRandom: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackRandom: {
        backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inactiveClassic: {
        fontSize: 18, color: 'white'
    },
    activeClassic: {
        fontSize: 18, color: 'white', borderColor: '#F11F43', borderBottomWidth: 4
    },

    activeChallenge: {
        fontSize: 18, color: 'white', borderColor: '#00C0FF', borderBottomWidth: 4
    },

    challengeActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#F11F43',
        width: 150
    },
    classicActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#00C0FF',
        width: 150
    },
    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },
    competitionText: {
        color: '#139BFF',
        fontWeight: '900',
        fontSize: 30,

    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        marginTop: 10,
        position: 'absolute',
        zIndex: 1000
    },
    nextText: {
        fontWeight: '800', fontSize: 20, color: '#fff'
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15,
        paddingTop: 2
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        paddingHorizontal: 35,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },

    textCenter: {
        textAlign: 'center'
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 30,
        textDecorationLine: 'underline',

    },

});
