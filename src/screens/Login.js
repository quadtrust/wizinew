import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native';
// import Button from 'react-native-button';
import { CheckboxField } from 'react-native-checkbox-field';
import { connect } from 'react-redux';
import { addFbToken } from '../actions/';

const FBSDK = require('react-native-fbsdk');

const {
  LoginManager,
  LoginButton,
  AccessToken
} = FBSDK;

class Login extends Component {

  fbAuth() {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      (result) => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              console.log(data.accessToken.toString());
              console.log('Data: ', data);
              // Send the data to the action.
              this.props.addFbToken(data.accessToken.toString());
            });
        }
      },
      (error) => {
        console.log(`Login fail with error: ${error}`);
      }
    );
  }


  dummy() {
    console.log('Pressed on google!!');
    this.props.navigation.navigate('Register');
  }
  render() {
      return (
          <View style={styles.fullWidth}>
            <View style={[styles.fullWidth]}>
              <Image
                  style={[styles.fullWidth, styles.notKnown, styles.contain, { margin: 10 }]}
                  source={require('../assets/images/wizi.png')}
              />
              <View style={{ flex: 3, justifyContent: 'space-between' }}>

                {/* <LoginButton
          publishPermissions={['publish_actions']}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("login has error: " + result.error);
              } else if (result.isCancelled) {
                alert("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    alert(data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => alert('logout.')} /> */}

                <View>
                  <View
                   style={[styles.socialButton,
                    styles.googleColor,
                    styles.margins]}
                  >
                    <TouchableOpacity onPress={() => { this.dummy(); }}>
                      <Text style={[styles.whiteColor, styles.blackColor]}>
                       SIGN IN WITH GOOGLE
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View
                   style={[styles.socialButton,
                    styles.facebookColor,
                    styles.margins]}
                  >
                    <TouchableOpacity onPress={() => { this.fbAuth(); }}>
                      <Text style={[styles.whiteColor, styles.blackColor]}>
                        SIGN IN WITH FACEBOOK
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View>
                  <View
                    style={{
                      left: 65,
                      marginTop: 7,
                      height: 1,
                      width: 100,
                      backgroundColor: 'white',
                      position: 'absolute'
                  }}
                  />
                  <Text
                      style={[styles.blackColor, styles.center]}
                  >OR</Text>
                  <View
                    style={{
                      marginTop: 7,
                      height: 1,
                      width: 100,
                      backgroundColor: 'white',
                      position: 'absolute',
                      right: 65
                  }}
                  />
                </View>
                <View>
                  <Text
                      style={[styles.blackColor, styles.center, styles.someMarginTop]}
                  >SIGN IN WITH YOUR EMAIL</Text>
                </View>
                <View>
                  <TextInput
                      style={[styles.field, styles.whiteColor, styles.margins]}
                      placeholder={'EMAIL ADDRESS'}
                      placeholderTextColor="white"
                      underlineColorAndroid="transparent"
                      keyboardType="email-address"
                      // onChangeText={email => this.setState({ email })}
                      // value={this.state.email}
                      returnKeyType="next"
                      onSubmitEditing={() => this.focusNextField('password')}

                  />
                  <TextInput
                      ref="password"
                      style={[
                        styles.field,
                        styles.whiteColor,
                        styles.margins,
                        styles.someMarginTop]}
                      placeholder={'PASSWORD'}
                      secureTextEntry
                      placeholderTextColor="white"
                      underlineColorAndroid="transparent"
                      // value={this.state.password}
                      // onChangeText={password => this.setState({ password })}
                      returnKeyType="go"
                  />

                  <View style={{ flexDirection: 'row', marginHorizontal: 50 }}>
                      <CheckboxField
                          onSelect={() => {
                              // this.setState({
                              //     remember: !this.state.remember
                              // });
                              console.log('checkbox checked');
                          }}
                          // selected={this.state.remember}
                          defaultColor='#fff'
                          selectedColor="#000"
                          containerStyle={{
                              flex: 1,
                              flexDirection: 'row',
                              alignItems: 'center',
                              paddingVertical: 20
                          }}
                          checkboxStyle={{ width: 20,
                              height: 20,
                              borderWidth: 2,
                              borderColor: '#fff',
                              borderRadius: 5 }}
                          labelSide="right"
                      >
                          <Text style={{ color: '#fff' }}>✓</Text>
                      </CheckboxField>
                      <View style={{ justifyContent: 'center', marginLeft: 5 }}>
                          <Text style={[styles.blackColor]}>REMEMBER ME?</Text>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.props.doLoad('forgotPassword')}
                        style={{
                          justifyContent: 'center',
                          alignItems: 'flex-end',
                          flex: 1
                        }}
                      >
                          <Text style={[styles.blackColor]}>FORGOT PASSWORD?</Text>
                      </TouchableOpacity>
                  </View>


                  <View style={styles.margins}>
                  <View
                   style={[styles.socialButton,
                     styles.createAccountColor,
                     styles.someMarginTop]}
                  >
                    <TouchableOpacity>
                      <Text style={[styles.whiteColor, styles.blackColor]}>SIGN IN</Text>
                    </TouchableOpacity>
                  </View>
                  <View
                   style={[styles.socialButton,
                     styles.createAccountColor,
                     styles.someMarginTop]}
                  >
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Register'); }}>
                      <Text style={[styles.whiteColor, styles.blackColor]}>CREATE ACCOUNT</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                </View>
                <View>
                  <Text
                      style={[styles.blackColor, styles.center, styles.botMar]}
                  >BY SIGNING UP YOU HAVE ACCEPTED OUR TERMS AND CONDITIONS</Text>
                </View>
              </View>
            </View>
          </View>

      );
  }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 20,
        alignItems: 'center'
    },
    margins: {
        marginLeft: 50,
        marginRight: 50,
    },
    googleColor: {
        backgroundColor: '#d54a33'
    },
    facebookColor: {
        backgroundColor: '#3160e4'
    },
    whiteColor: {
        color: 'white'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#f44f4b',
        borderColor: 'white',
        borderWidth: 1,
        padding: 5
    },
    createAccountColor: {
        backgroundColor: '#1fd2cc'
    },
    someMarginTop: {
        marginTop: 5
    },
    botMar: {
        marginBottom: 10
    }
});

const mapStateToProps = (state) => ({
  username: state.auth.user.name
});

export default connect(mapStateToProps, { addFbToken })(Login);
