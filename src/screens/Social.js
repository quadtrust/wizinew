import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Linking
} from 'react-native';
import Header from '../components/Header';

export default class Social extends Component {
  render() {
    return (
      <View style={styles.fullWidth}>
        <Header title={'SOCIAL'} />
        <View style={styles.bodyWidth}>
          <Text style={[styles.textCenter, styles.headerText]}>
            SHARE ON SOCIAL MEDIA
          </Text>
          <View style={[styles.center]}>
            <Image
              style={[styles.contain, { width: 350, height: 125 }]}
               source={require('../assets/images/wizi.png')}
            />
            <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/sharer/sharer.php?u=http%3A//meteoricvisions.com/')}>
              <Image
                style={[styles.contain, styles.socialButtonSize]}
                source={require('../assets/images/social/fb.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL('https://pinterest.com/pin/create/button/?url=http%3A//meteoricvisions.com/&media=http%3A//meteoricvisions.com/images/theme/wizi.png')}>
              <Image
                style={[styles.contain, styles.socialButtonSize]}
                source={require('../assets/images/social/pinterest.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/home?status=http%3A//meteoricvisions.com/')}>
              <Image
                style={[styles.contain, styles.socialButtonSize]}
                source={require('../assets/images/social/twitter.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => Linking.openURL('https://plus.google.com/share?url=http%3A//meteoricvisions.com/')}>
              <Image
                style={[styles.contain, styles.socialButtonSize]}
                source={require('../assets/images/social/google.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  fullWidth: {
    flex: 1
  },
  headerText: {
    color: '#000',
    fontFamily: 'mikadoultra',
    fontSize: 22,
    marginTop: 40
  },
  contain: {
    resizeMode: 'contain'
  },
  socialButtonSize: {
    width: 180, height: 50, marginTop: 15
  },
  bodyWidth: {
    flex: 18,
    backgroundColor: '#fff'
  },
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  textCenter: {
    textAlign: 'center'
  }
});
