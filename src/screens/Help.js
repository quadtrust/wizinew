import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Header from '../components/Header';

export default class Help extends Component {
    render() {
        return (
            <View style={styles.fullWidth}>
                <Header title={'WIZI QUIZ'} />
                <View style={styles.bodyWidth}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.headingText}>
                            HELP
                        </Text>
                    </View>
                    <View style={styles.listContainer}>
                        <TouchableOpacity>
                            <Text style={styles.listText}>
                                LEARN HOW TO PLAY
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.listContainer}>
                        <TouchableOpacity>
                            <Text style={styles.listText}>
                                RULE BOOK
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.listContainer}>
                        <TouchableOpacity>
                            <Text style={styles.listText}>
                                SHOP
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.listContainer}>
                        <TouchableOpacity>
                            <Text style={styles.listText}>
                                TERMS OF USE
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.listContainer}>
                        <TouchableOpacity>
                            <Text style={styles.listText}>
                                PRIVACY POLICY
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    bodyWidth: {
        flex: 18,
        backgroundColor: '#fff'
    },
    headingContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 25
    },
    headingText: {
      fontSize: 30,
      color: 'skyblue',
      fontFamily: 'mikadoultra',
      marginTop: 25
    },
    listText: {
      color: '#000',
      fontSize: 20,
      fontFamily: 'mikadoultra',
      paddingLeft: 20
    },
    listContainer: {
      borderBottomWidth: 2,
      borderColor: '#D1D2D2',
      paddingBottom: 25,
      paddingTop: 25
    }
});
