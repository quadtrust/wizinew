import React, { Component } from 'react';
import { View,
    Text,
    Image,
    ScrollView,
    TextInput,
    StyleSheet,
    TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Picker from 'react-native-picker';
import RadioForm from 'react-native-simple-radio-button';
import { inputUpdate, submitRegister } from '../actions';

    const radioProps = [
      { label: 'BOY', value: 0 },
      { label: 'GIRL', value: 1 }
    ];

    const data = [];
    for (let i = 0; i < 10; i++) {
      data.push(i);
    }

    // var RadioButtonProject = React.createClass({
    //   getInitialState: function() {
    //     return {
    //       value: 0,
    //     }
    //   },
    //   render: function() {
    //     return (
    //       <View>
    //         <RadioForm
    //           radio_props={radio_props}
    //           initial={0}
    //           onPress={(value) => {this.setState({value:value})}}
    //         />
    //       </View>
    //     );
    //   }
    // });

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      first_name: '',
      last_name: '',
      language: '',
      age: 0,
      dob: null,
      school: '',
      standard: null,
      directorate: null,
      password: null,
      password_confirmation: null,
      gender: 0
    };

    this.submit = this.submit.bind(this);
    this.inputUpdate = this.inputUpdate.bind(this);
  }

  inputUpdate({ prop, value }) {
    this.setState({ [prop]: value });
  }
  showDirectoratePicker() {
    Picker.init({
            pickerData: data,
            selectedValue: [5],
            onPickerConfirm: (d) => {
                console.log(d);
            },
            pickerTitleText: 'CHOOSE LANGUAGE',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
  }

  showLanguagePicker() {
    Picker.init({
            pickerData: ['ENGLISH', 'ARABIC'],
            selectedValue: ['ENGLISH'],
            onPickerConfirm: () => {
                console.log('data');
            },
            pickerTitleText: 'CHOOSE LANGUAGE',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
  }

  showStandardPicker() {
    Picker.init({
            pickerData: ['ENGLISH', 'ARABIC'],
            selectedValue: ['ENGLISH'],
            onPickerConfirm: () => {
                console.log('data');
            },
            pickerTitleText: 'CHOOSE LANGUAGE',
            pickerConfirmBtnText: 'CHOOSE',
            pickerCancelBtnText: 'CANCEL'
        });
        Picker.show();
  }
  submit() {
    const token = this.props.token;
    const user = {};
    user.first_name = this.state.first_name;
    user.last_name = this.state.last_name;
    user.username = this.state.username === '' ? this.props.username : this.state.username;
    user.email = this.state.email === '' ? this.props.email : this.state.email;
    user.dob = this.state.dob;
    user.school = this.state.school;
    user.standard = this.state.standard;
    // user.directorate = this.state.directorate;
    user.age = this.state.age;
    user.gender = this.state.gender;
    user.password = this.state.password;
    user.password_confirmation = this.state.password_confirmation;
    this.props.submitRegister(user, token);
  }
  render() {
      return (
          <View style={styles.fullWidth}>
              <Image
                  style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                  source={require('../assets/images/register/back.png')}
              >
                  <Text
                      style={[
                        { textAlign: 'center', fontSize: 28, fontFamily: 'mikadoultra', paddingTop: 20 },
                         styles.white]}
                  >
                     REGISTER BELOW
                  </Text>
              </Image>
              <View style={{ flex: 3, justifyContent: 'space-between', marginTop: 10 }}>
                  <ScrollView>
                      <View>

                          <TextInput
                              ref="1"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 20 }]}
                              placeholder='FIRST NAME'
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              keyboardType="default"
                              // editable={false}
                              onChangeText={value => this.inputUpdate({ prop: 'first_name', value })}
                              // value={this.state.firstName}
                              // onSubmitEditing={() => this.focusNextField('2')}
                          />
                          <TextInput
                              ref="2"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='LAST NAME'
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'last_name', value })}
                              // value={this.state.lastName}
                              // onSubmitEditing={() => this.focusNextField('3')}
                          />
                          <TextInput
                              ref="3"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='USERNAME'
                              disabled={this.props.username !== ''}
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'username', value })}
                              value={this.props.username === '' ? this.state.username : this.props.username}
                          />
                          <Text>{this.state.username}</Text>
                          <TouchableOpacity
                           style={[styles.field, styles.margins,
                              { marginTop: 10, justifyContent: 'center' }]}
                               onPress={this.showLanguagePicker.bind(this, {})}
                          >
                                      <Text style={{ fontWeight: '800', color: '#808080' }}>
                                      CHOOSE LANGUAGE</Text>
                          </TouchableOpacity>

                          <TextInput
                              ref="5"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='AGE'
                              placeholderTextColor="grey"
                              keyboardType="phone-pad"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'age', value })}
                              // value={this.state.age}
                              // onSubmitEditing={() => _dobField.focus()}
                          />
                          <TextInput
                              // ref={(dob) => {
                              //     _dobField = dob;
                              // }}
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='DATE OF BIRTH'
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'dob', value })}
                              // value={this.state.dob}
                              // onFocus={this.showPicker.bind(this, {})}
                          />

                          <TextInput
                              ref="7"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='NAME OF YOUR SCHOOL'
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'school', value })}
                              // value={this.state.school}
                              // returnKeyType="next"
                          />

                          <TouchableOpacity
                          style={[styles.field, styles.margins,
                             { marginTop: 10, justifyContent: 'center' }]}
                              onPress={this.showStandardPicker.bind(this, {})}
                          >
                                      <Text
                                       style={{ fontWeight: '800', color: '#808080' }}
                                      >
                                       CHOOSE STANDARD</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                          style={[styles.field, styles.margins,
                             { marginTop: 10, justifyContent: 'center' }]}
                              onPress={this.showDirectoratePicker.bind(this, {})}
                          >
                                      <Text style={{ fontWeight: '800', color: '#808080' }}>
                                      CHOOSE DIRECTORATE</Text>
                          </TouchableOpacity>

                        {/*  <Picker
                              selectedValue={this.state.standard}
                              mode='dropdown'
                              style={[styles.field, styles.black, styles.margins,
                               { marginTop: 10, color: 'grey' }]}
                              onValueChange={(standard) => this.setState({ standard })}>

                              {this.state.availableStandards.map((standard, index) => {
                                  return <Picker.Item key={standard.id} label={standard.class}
                                   value={standard.class} />;
                              })}

                          </Picker>

                          <Picker
                              selectedValue={this.state.standard}
                              mode='dropdown'
                              style={[styles.field, styles.black, styles.margins,
                               { marginTop: 10, color: 'grey' }]}
                              onValueChange={(directorate) => this.setState({ directorate })}>

                              {this.state.availableDirectorates.map((directorate, index) => {
                                  return <Picker.Item key={directorate.id} label={directorate.name}
                                   value={directorate.id} />;
                              })}

                          </Picker> */ }

                          <TextInput
                              ref="10"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='EMAIL ADDRESS'
                              keyboardType="email-address"
                              disabled={this.props.email !== ''}
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'email', value })}
                              value={this.props.email === '' ? this.state.email : this.props.email}
                              returnKeyType="next"
                            /*  onSubmitEditing={() => this.focusNextField('11')} */
                          />
                          <TextInput
                              ref="11"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='PASSWORD'
                              placeholderTextColor="grey"
                              underlineColorAndroid="transparent"
                              secureTextEntry
                              onChangeText={value => this.inputUpdate({ prop: 'password', value })}
                              // value={this.state.password}
                              // returnKeyType="next"
                            /*  onSubmitEditing={() => this.focusNextField('12')} */
                          />
                          <TextInput
                              ref="12"
                              style={[styles.field, styles.black, styles.margins,
                                 { fontWeight: '800', marginTop: 10 }]}
                              placeholder='CONFIRM PASSWORD'
                              // placeholderTextColor="grey"
                              secureTextEntry
                              underlineColorAndroid="transparent"
                              onChangeText={value => this.inputUpdate({ prop: 'password_confirmation', value })}

                              // value={this.state.password_confirm}
                              returnKeyType="done"
                          />
                      </View>
                      <View>
                          <RadioForm
                              style={[styles.margins, {
                              marginTop: 10,
                              alignItems: 'center',
                              justifyContent: 'center'
                          }]}
                              radio_props={radioProps}
                              initial={0}
                              formHorizontal
                              labelHorizontal
                              buttonColor={'#F44E4B'}
                              animation
                              onPress={(value) => {
                                  // this.setState({ value: value });
                                  // this._gender(value);
                                  console.log(value);
                              }}
                              labelStyle={{ padding: 10 }}
                          />
                      </View>
                      <View>
                          <TouchableOpacity
                              onPress={() => this.submit()}
                              style={[styles.socialButton, styles.red, {
                                  marginTop: 20,
                                  marginLeft: 120,
                                  marginRight: 120,
                                  justifyContent: 'center',
                                  alignItems: 'center'
                              }]}
                          ><Text
                            style={[{ fontSize: 18, color: 'white' }]}
                          >
                          REGISTER</Text>
                          </TouchableOpacity>
                      </View>

                  </ScrollView>
              </View>

          </View>
      );
  }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontWeight: '700',
        fontSize: 23,
        color: '#00AEFF',

    },
    selected_header: {
        fontWeight: '700',
        fontSize: 25,
        color: '#00AEFF',

    },
    challenge_header: {
        fontWeight: '900',
        fontSize: 35,
        color: '#ffffff',

    },
    challenge_sub_header: {
        fontWeight: '100',
        fontSize: 23,
        color: '#ffffff',

    },
    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 4,
        resizeMode: 'stretch',
        marginTop: 20,
        marginHorizontal: 20
    },
    whiteBack: {
        backgroundColor: '#FEFFFF'
    },
    center_element: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    languageHeader: { fontWeight: '700', fontSize: 23, color: 'white', textAlign: 'center' },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    challengeButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackFriends: {
      backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    activeBackRandom: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackRandom: {
        backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inactiveClassic: {
        fontSize: 18, color: 'white'
    },
    activeClassic: {
        fontSize: 18, color: 'white', borderColor: '#F11F43', borderBottomWidth: 4
    },

    activeChallenge: {
        fontSize: 18, color: 'white', borderColor: '#00C0FF', borderBottomWidth: 4
    },

    challengeActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#F11F43',
        width: 150
    },
    classicActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#00C0FF',
        width: 150
    },
    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },
    competitionText: {
        color: '#139BFF',
        fontWeight: '900',
        fontSize: 30,

    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        marginTop: 10,
        position: 'absolute',
        zIndex: 1000
    },
    nextText: {
        fontWeight: '800', fontSize: 20, color: '#fff'
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15,
        paddingTop: 2
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        paddingHorizontal: 35,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },

    textCenter: {
        textAlign: 'center'
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 30,
        textDecorationLine: 'underline',

    },

});
const mapStateToProps = (state) => {
  return {
    email: state.auth.user.email || '',
    username: state.auth.user.name || '',
    token: state.auth.user.fbToken || '8358253943554'
  };
};

export default connect(mapStateToProps, { inputUpdate, submitRegister })(Register);
