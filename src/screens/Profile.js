import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity } from 'react-native';
  import { connect } from 'react-redux';


import Button from 'react-native-button';
import Header from '../components/Header';
import { getPlayer } from '../actions';
// import API from './API'
// import moment from 'moment'

 class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // username: 'Mritunjay',
      welcome: 'Hello',
      dob: '30 JULY 1993',
      country: 'India',
      playerpic: 0,
      number: '',
      school: '',
      email: '',
      editable: false,
      mobile: '',
      avatar: require('../assets/images/user.png'),
      questions_attempted: 0,
      tests_attempted: 0,
      overall_points: 0
    };
  }

  componentWillMount() {
    this.props.getPlayer(this.props.userid);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.player !== undefined) {
      this.setState({ playerpic: nextProps.player.profile_pic });
    switch (nextProps.player.profile_pic) {
      case 1: {
        this.setState({ avatar: require('../assets/human/human1.png') });

        break;
      }
      case 2: {
        this.setState({ avatar: require('../assets/human/human2.png') });

        break;
      }

      case 3: {
        this.setState({ avatar: require('../assets/human/human3.png') });

        break;
      }

      case 4: {
        this.setState({ avatar: require('../assets/human/human4.png') });

        break;
      }
      case 5: {
        this.setState({ avatar: require('../assets/human/human5.png') });

        break;
      }
      case 6: {
        this.setState({ avatar: require('../assets/human/human6.png') });

        break;
      }
      case 7: {
        this.setState({ avatar: require('../assets/human/human7.png') });

        break;
      }
      case 8: {
        this.setState({ avatar: require('../assets/human/human8.png') });

        break;
      }
      case 9: {
        this.setState({ avatar: require('../assets/human/human9.png') });

        break;
      }
      case 10: {
        this.setState({ avatar: require('../assets/human/human10.png') });

        break;
      }
      case 11: {
        this.setState({ avatar: require('../assets/human/human11.png') });

        break;
      }
      case 12: {
        this.setState({ avatar: require('../assets/human/human12.png') });

        break;
      }
      default: {
        this.setState({ avatar: require('../assets/images/user.png') });
      }
    }
    }
  }


  editOrSaveProfile() {
    if (this.state.editable) {
      this.setState({
        editable: false
      });
    } else {
      this.setState({
        editable: true
      });
    }
  }


  render() {
    let usernameField;
     let dobField;
    let countryField;
      let schoolField;
      let buttonText;
      let mobileNumber;
      let labelPadding;
    if (this.state.editable) {
      usernameField = (
        <TextInput
        ref='first_field'
        style={{ color: 'white', height: 30, padding: 5, margin: 0 }}
        value={this.state.username}
        onChangeText={(username) => this.setState({ username })}
        />
      );
      dobField = (
          <TextInput
          style={{ color: 'white', height: 30, padding: 5, margin: 0 }}
          value={this.state.dob} onChangeText={(dob) => this.setState({ dob })}
          />
      );
    countryField = (
        <TextInput
        style={{ color: 'white', height: 30, padding: 5, margin: 0 }}
         value={this.state.country}
         onChangeText={(country) => this.setState({ country })}
        />
      );
      schoolField = (
        <TextInput
         style={{ color: 'white', height: 30, padding: 5, margin: 0 }}
         value={this.state.school}
         onChangeText={(school) => this.setState({ school })}
        />
       );
      mobileNumber = (
            <TextInput
            style={{ color: 'white', height: 30, padding: 5, margin: 0 }}
            value={this.state.number}
            onChangeText={(number) => this.setState({ number })}
            />
        );
        buttonText = 'SAVE PROFILE';
      } else {
        usernameField = (
          <Text
            style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
          > {this.props.player.username}</Text>
        );
        dobField = (
        <Text
        style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
        >
        {this.state.dob}</Text>);
        countryField = (
        <Text
        style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
        > {this.state.country}</Text>
      );
      schoolField = (
        <Text
        style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
        > {this.state.school}</Text>
      );
      mobileNumber = (
        <Text
        style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
        > {this.state.number}</Text>
      );
        buttonText = 'EDIT PROFILE';
    }

    return (
      <View style={styles.head}>
          <Header title={'WIZI QUIZ'} />
          <View style={[styles.body]}>
              <ScrollView>
                  <Text
                    style={[styles.h1, styles.padVertical,
                       styles.white, styles.textCenter,
                        styles.bold, styles.backGreen]}
                  >PROFILE</Text>
                  <Text
                    style={[styles.textCenter,
                       styles.white, styles.padVertical,
                        styles.bold, styles.h1, styles.h2, styles.backBlue]}
                  >
                      WELCOME {this.state.welcome}</Text>
                  <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                      <View style={[styles.row]}>
                          <TouchableOpacity onPress={() => this.props.navigation.navigate('ChooseCharacter')}>
                              <Image
                               source={this.state.avatar}
                               resizeMode={'contain'}
                               style={{ height: 135, width: 150 }}
                              />
                          </TouchableOpacity>
                          <View style={[styles.center]}>
                              <Button
                                containerStyle={[styles.button1, { marginLeft: 20 }]}
                                style={[styles.textCenter,
                                   styles.bold, { color: 'white', marginTop: 2 }]}
                                onPress={() => {
                                            this.editOrSaveProfile();
                                        }}
                              >
                                {buttonText}
                              </Button>
                              <Button
                                containerStyle={[styles.button2, { marginLeft: 20 }]}
                                style={[styles.textCenter, { color: 'white', marginTop: 2 }]}
                              >
                                  EDIT PASSWORD
                              </Button>
                          </View>

                      </View>

                      <View style={[{ marginTop: 20 }, styles.row]}>
                          <View style={[styles.padVertical, { flex: 1, justifyContent: 'center' }]}>
                              <Text
                              style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > USERNAME:</Text>
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > DOB:</Text>
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > COUNTRY:</Text>
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > MOBILE:</Text>
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > SCHOOL:</Text>
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont, styles.marginBottom]}
                              > EMAIL:</Text>
                          </View>
                          <View style={[styles.padVertical, { flex: 2 }]}>
                            {usernameField}
                            {dobField}
                            {countryField}
                            {mobileNumber}
                            {schoolField}
                              <Text
                               style={[styles.white, styles.bold, styles.mediumFont]}
                              > {this.state.email}</Text>
                          </View>
                      </View>


                  </View>
                  <View style={[styles.backFb, styles.center, { marginTop: 10 }]}>
                      <Text style={[styles.white, styles.bold, styles.h1, styles.padVertical]}>
                          SUMMARY
                      </Text>
                  </View>
                  <View style={[styles.row]}>
                      <View
                       style={[styles.center,
                          { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}
                      >
                          <Text
                           style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}
                          >
                              QUESTION ATTEMPED
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.questions_attempted}
                          </Text>
                      </View>
                      <View
                       style={[styles.center,
                          { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}
                      >
                          <Text
                           style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}
                          >
                              TESTS ATTEMPED
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.tests_attempted}
                          </Text>
                      </View>
                      <View
                       style={[styles.center,
                          { flex: 2, marginTop: 5, borderLeftWidth: 2, borderColor: '#424041' }]}
                      >
                          <Text
                           style={[styles.white, styles.bold, styles.textCenter, styles.mediumFont]}
                          >
                              OVERALL POINTS
                          </Text>
                          <Text style={[styles.yellow, styles.bold, styles.h1]}>
                            {this.state.overall_points}
                          </Text>
                      </View>
                  </View>
              </ScrollView>
          </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  marginBottom: {
    marginBottom: 5
  },
  head: {
    flex: 1,
    backgroundColor: '#2B292A'
  },
  button1: {
    backgroundColor: '#ED145B', width: 150, height: 30,
  },
  button2: {
    backgroundColor: '#ED145B', width: 150, height: 30, marginTop: 20,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row'
  },

  body: {
    flex: 18
  },
  backGreen: {
    backgroundColor: '#3DBAA6'
  },
  backBlue: { backgroundColor: '#3FC1FF' },
  backFb: { backgroundColor: '#4276BA' },
  yellow: { color: '#FCFE00' },
  white: {
    color: 'white'
  },
  padVertical: {
    paddingTop: 5,
    paddingBottom: 5
  },
  h1: {
    fontSize: 30
  },
  mediumFont: {
    fontSize: 17
  },
  h2: {
    fontSize: 20
  },
  textCenter: {
    textAlign: 'center'
  },
  bold: {
    fontWeight: '800'
  },
  black: {
    color: '#000'
  },

  margins: {
    marginLeft: 50,
    marginRight: 50
  },
});
const mapStateToProps = (state) => ({
  userid: state.auth.user.fbId,
  player: state.auth.player,
});
export default connect(mapStateToProps, { getPlayer })(Profile);
