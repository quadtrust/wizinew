import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import PlayerStats from '../components/PlayerStats';
import Header from '../components/Header';
import { subjectSelection } from '../actions';

class SubjectSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }
  selectedSubject(subject) {
    this.props.subjectSelection(subject);
  }
  render() {
    return (
      <View style={styles.fullWidth}>
        <Header title={'Subjects'} />
        <PlayerStats spins={this.props.spinsCount} />
        <View style={[{ flex: 18, justifyContent: 'center', marginBottom: 10 }]}>
          <Text style={[styles.textCenter, styles.heading]}>SELECT YOUR SUBJECT</Text>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity
                onPress={() => { this.selectedSubject('SOCIAL STUDIES'); }}
                  style={[this.state.active ? styles.activeBack : styles.notActive, {
                      flex: 1,
                      padding: 5
                  }]}
              >
                  <Image
                    style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                    resizeMode='contain'
                    source={require('../assets/images/subject/socia.png')}
                  />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => { this.selectedSubject('SCIENCE'); }}
                style={[styles.notActive, {
                    flex: 1,
                    padding: 5
                }]}
              >
                <Image
                  style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                  resizeMode='contain'
                  source={require('../assets/images/subject/science.png')}
                />
              </TouchableOpacity>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity
                onPress={() => { this.selectedSubject('MATHS'); }}
                style={[styles.notActive, {
                    flex: 1,
                    padding: 5
                }]}
              >
              <Image
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                resizeMode='contain'
                source={require('../assets/images/subject/maths.png')}
              />
              </TouchableOpacity>
              <TouchableOpacity
               onPress={() => { this.selectedSubject('ARABIC'); }}
                  style={[styles.notActive, {
                      flex: 1,
                      padding: 5
                  }]}
              >
                <Image
                  style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                  resizeMode='contain'
                  source={require('../assets/images/subject/arabic.png')}
                />
              </TouchableOpacity>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={() => { this.selectedSubject('ENGLISH'); }}
                style={[styles.notActive, {
                    flex: 1,
                    padding: 5
                }]}
            >
                <Image
                  style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                  resizeMode='contain'
                  source={require('../assets/images/subject/english.png')}
                />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.selectedSubject('ISLAMIC STUDIES'); }}
              style={[styles.notActive, {
                  flex: 1,
                  padding: 5
              }]}
            >
            <Image
              style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
              resizeMode='contain'
              source={require('../assets/images/subject/islam.png')}
            />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
           onPress={() => { console.log('start game'); }}
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <Image
              style={[styles.contain, { height: 35, width: 120, marginTop: 10 }]}
              source={require('../assets/images/subject/start.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontFamily: 'mikadoultra',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },
    textShadow: {
        textShadowColor: 'black',
        textShadowOffset: { width: 5, height: 5 },
        textShadowRadius: 15
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: 'white'
    },

    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
});

const mapStateToProps = (state) => ({
  spinsCount: state.gamequestion.spinsCount,
});

export default connect(mapStateToProps, { subjectSelection })(SubjectSelection);
