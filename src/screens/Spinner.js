import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, Image, Animated, StyleSheet, Vibration } from 'react-native';
import Header from '../components/Header';
import PlayerStats from '../components/PlayerStats';
import { subjectSelection, reduceSpin, getPlayer } from '../actions';
import Sounds from '../services/Sounds';

let rotation;
let lastSpin = 0;
class Spinner extends Component {
  constructor(props) {
    super(props);

    this.state = {
        subjectRotation: new Animated.Value(0),
        spinning: false,
        spinsCount: 0
    };
  }

  componentWillMount() {
    this.props.getPlayer(this.props.userid);
  }

  _spin() {
        if (!this.state.spinning) {
            this.setState({
              spinning: true
            });

            let subjectsRotate = Math.floor(Math.random() * 360) + 1;
            if (
                (subjectsRotate >= 31 && subjectsRotate <= 36) ||
                (subjectsRotate >= 91 && subjectsRotate <= 96) ||
                (subjectsRotate >= 149 && subjectsRotate <= 155) ||
                (subjectsRotate >= 210 && subjectsRotate <= 216) ||
                (subjectsRotate >= 269 && subjectsRotate <= 275) ||
                (subjectsRotate >= 329 && subjectsRotate <= 335)
            ) {
                subjectsRotate += 6;
            }
            subjectsRotate += (3 * 360);
            subjectsRotate += lastSpin;
            lastSpin = subjectsRotate;
            if (this.props.spinsCount === 0) {
              console.log('NO SPINs LIMIT');
                // ToastAndroid.show('Sorry you cannot spin more', ToastAndroid.SHORT);
            } else {
              if (this.props.music) {
                // Sounds.pauseBackground();
                Sounds.playSpinner();
              }
              if (this.props.vibration) {
                console.log('Vibrating...');
                Vibration.vibrate([0, 90, 90, 0]);
              }
                console.log('Spinnig', this.props.spinsCount);
                this.props.reduceSpin(this.props.spinsCount);
                rotation = '';
                this.state.subjectRotation.addListener((value) => {
                    rotation = JSON.stringify(value.value) % 360;
                });

                Animated.timing(
                    this.state.subjectRotation,
                    {
                        toValue: subjectsRotate,
                        duration: 3000
                    }
                ).start((resp) => {
                    if (resp.finished) {
                        this.setState({ spinning: false });
                        if (rotation > 36 && rotation <= 91) {
                            this.props.subjectSelection('MATHS');
                        } else if (rotation > 96 && rotation <= 149) {
                            this.props.subjectSelection('SOCIAL STUDIES');
                        } else if (rotation > 155 && rotation <= 210) {
                            this.props.subjectSelection('ISLAMIC STUDIES');
                        } else if (rotation > 216 && rotation <= 269) {
                            this.props.subjectSelection('SCIENCE');
                        } else if (rotation > 275 && rotation <= 329) {
                            this.props.subjectSelection('ENGLISH');
                        } else {
                            this.props.subjectSelection('ARABIC');
                        }
                    }
                });
            }
        }
    }

  render() {
    const spin = this.state.subjectRotation.interpolate({
            inputRange: [0, 360],
            outputRange: ['0deg', '360deg']
        });
    return (
      <View style={styles.fullWidth}>
        <Header title={'Spinner'} />
        <PlayerStats spins={this.props.spinsCount} />
        <View style={[styles.bodyWidth, styles.whiteBackground]}>
          <Text style={styles.spinText}>
            SPIN THE WIZI WHEEL
          </Text>
          <View style={{ flex: 3 }}>
              <Animated.Image
                  source={require('../assets/images/spinner/subjects.png')}
                  style={{
                      flex: 1,
                      height: undefined,
                      width: undefined,
                      resizeMode: 'contain',
                      margin: 26,
                      zIndex: -1,
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      bottom: 0,
                      left: 0,
                      transform: [{
                          rotate: spin
                      }]
                  }}
              />

              <Image
                  source={require('../assets/images/spinner/circumference.png')}
                  style={{
                      flex: 1,
                      height: undefined,
                      width: undefined,
                      resizeMode: 'contain',
                      margin: 15,
                      zIndex: -1,
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      bottom: 0,
                      left: 0
                  }}
              />


            <TouchableOpacity
              style={{ flex: 1, margin: 115, zIndex: 1 }}
              onPress={() => this._spin()}
            >
                <Image
                 source={require('../assets/images/spinner/center.png')}
                 style={{ flex: 1,
                    height: undefined,
                    width: undefined,
                    resizeMode: 'contain' }}
                />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1.1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <View
              style={{
                flex: 1,
                marginTop: 30,
                marginBottom: 10
              }}
            >
            <TouchableOpacity style={{ flex: 1 }}>
              <Image
                source={require('../assets/images/common/spin.png')}
                style={{ height: undefined, width: undefined, resizeMode: 'contain', flex: 2 }}
              >
                <Text
                  style={{
                    marginTop: 16,
                    color: 'white',
                    textAlign: 'center',
                    fontWeight: 'bold'
                  }}
                >{this.props.spinsCount}</Text>
              </Image>
                <Text
                  style={{ flex: 1, color: 'white', textAlign: 'center', fontFamily: 'mikadoultra' }}
                >SPINS LEFT</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                marginTop: 30,
                marginBottom: 10
              }}
            >
            <TouchableOpacity
              style={{ flex: 1 }} onPress={() => this._spin()}
            >
              <Image
                source={require('../assets/images/spinner/spin-again.png')}
                style={{ height: undefined, width: undefined, resizeMode: 'contain', flex: 2 }}
              />
              <Text style={{ flex: 1, color: 'white', textAlign: 'center', fontFamily: 'mikadoultra' }}>SPIN AGAIN</Text>
            </TouchableOpacity>
          </View>
            <View style={{ flex: 1, marginTop: 30, marginBottom: 10, marginRight: 10 }}>
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => { console.log('redirect to shops screen'); }}
              >
                <Image
                  source={require('../assets/images/common/coin.png')}
                  style={{ height: undefined, width: 80, resizeMode: 'contain', flex: 2, marginLeft: 20 }}
                />
                <Text style={{ flex: 1, color: 'white', textAlign: 'center', fontFamily: 'mikadoultra' }}>BUY MORE SPINS</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    spinText: {
      fontFamily: 'mikadoultra',
      textAlign: 'center',
      fontSize: 30,
      // fontWeight: 'bold',
      color: 'white',
      padding: 20
    },
    fullWidth: {
        flex: 1
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: '#2B292A'
    }
});

const mapStateToProps = (state) => ({
    spinsCount: state.gamequestion.spinsCount,
    music: state.settings.music,
    vibration: state.settings.vibration,
    userid: state.auth.user.fbId
});

export default connect(mapStateToProps, { subjectSelection, reduceSpin, getPlayer })(Spinner);
