import React, { Component } from 'react';
import {
    View,
    TextInput,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
// import Button from 'react-native-button'

export default class ForgetPassword extends Component {
    render() {
        return (
            <View style={[styles.fullWidth, styles.backBlack, { justifyContent: 'center' }]}>
                <Text style={[styles.headline, styles.center]}>
                    FORGOT PASSWORD
                </Text>
                <View style={{ alignItems: 'center', marginTop: 20 }}>
                   <TextInput
                       ref="password"
                       style={[{
                           height: 35,
                           width: 300,
                           backgroundColor: '#EFE9E9',
                           borderColor: '#8F8C8D',
                           borderBottomWidth: 1,
                           padding: 5,
                           color: 'black',
                           borderRadius: 5 }]}
                       placeholder="Enter your email"
                       placeholderTextColor="black"
                       underlineColorAndroid="transparent"
                       returnKeyType="go"
                   />
               </View>
                <View style={{ alignItems: 'center' }}>
                  <View style={[styles.socialButton, styles.red, { width: 200, marginTop: 20 }]}>
                    <TouchableOpacity>
                      <Text style={[styles.whiteColor, styles.smallFont, { fontWeight: 'bold' }]}>Submit</Text>
                    </TouchableOpacity>
                  </View>
                    {/*<Button
                      containerStyle={[styles.socialButton, styles.red, styles.createAccountColor,{width:200,marginTop:20}]}
                      style={[styles.whiteColor, styles.smallFont]}
                    >Submit</Button>*/}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1,
    backgroundColor: '#fff'
  },
  backBlack: {
    backgroundColor: '#000001'
  },
  headline: {
    fontSize: 25,
    marginLeft: 20,
    marginRight: 20,
    color: 'orange',
    fontWeight: '800'
},
red: {
    backgroundColor: '#F44E4B'
},
whiteColor: { color: 'white' },
center: {
    textAlign: 'center'
},
socialButton: {
    padding: 10,
    marginBottom: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
},
smallFont: {
      fontSize: 12
  },
});
