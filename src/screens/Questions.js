import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Header from '../components/Header';
// import language from './Language'
export default class Question extends Component {

    render() {
        return (
            <View style={[styles.fullWidth]}>
              <Header title={'Suggest Q'} />
                <View style={[styles.bodyWidth]}>
                    <View style={[styles.questionContainer, { marginTop: 70 }]}>
                        <Text style={[styles.headline]}>
                            Add Question
                        </Text>
                        <TouchableOpacity
                         onPress={() => this.props.navigation.navigate('SuggestQuestion')}
                        >
                            <Text style={[styles.subHeader]}>Have Question</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.questionContainer]}>
                        <Text style={[styles.headline]}>
                            Check Status
                        </Text>
                        <TouchableOpacity >
                            <Text style={[styles.subHeader]}>Check Message Status</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.questionContainer]}>
                        <Text style={[styles.headline]}>
                            Rate Questions
                        </Text>
                        <TouchableOpacity>
                            <Text style={[styles.subHeader]}>
                                You can now rate the questions which are available on our database.
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    questionContainer: {

        borderBottomWidth: 2,
        borderColor: 'white',
        paddingBottom: 30,
        marginTop: 20
    },
    contain: {
        resizeMode: 'contain'
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontFamily: 'mikadoultra'
    },
    subHeader: {
        fontSize: 23,
        marginLeft: 20,
        marginRight: 20,
        fontFamily: 'mikadoultra',
        color: 'white'
    },
    socialButtonSize: {
        width: 180, height: 50, marginTop: 15
    },
    bodyWidth: {
        flex: 18,
        backgroundColor: '#2B292A'
    },
    row: {
        flexDirection: 'row'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    }
});
