import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    LayoutAnimation,
    Vibration,
    ProgressBarAndroid
} from 'react-native';
import DialogBox from 'react-native-dialogbox';
import { connect } from 'react-redux';
import { questionFetch, showResult, unlockCharacter, fetchUnlockCharacters } from '../actions';
import Header from '../components/Header';
import PlayerStats from '../components/PlayerStats';
import Sounds from '../services/Sounds';

const { height, width } = Dimensions.get('screen');

const Game = {
  question_no: 0
};
let timer;
class Subject extends Component {

    constructor(props) {
        super(props);

        this.state = {
            lives: 3,
            correctAnswers: 0,
            incorrectAnswers: 0,
            question: {},
            correct_answer_back: require('../assets/images/common/2.png'),
            incorrect_answer_back: require('../assets/images/common/3.png'),
            currentUnlock: 0,
            progress: 0,
            questions: false,
            i: 0,

            answered: false,
            remark2: 'Second Remark',
            teacher: require('../assets/images/common/monster.png'),
            background: null,
            button1: require('../assets/images/common/1.png'),
            button2: require('../assets/images/common/1.png'),
            button3: require('../assets/images/common/1.png'),
            button4: require('../assets/images/common/1.png'),
            unlockCharacter: false
        };

        this.showCorrectORIncorrectButton = this.showCorrectORIncorrectButton.bind(this);
        this.nextQuestion = this.nextQuestion.bind(this);
        this.showResultButton = this.showResultButton.bind(this);
        this.showResult = this.showResult.bind(this);
    }

    componentWillMount() {
      this.props.questionFetch(this.props.subject);
      this.props.fetchUnlockCharacters(this.props.player_id);
      switch (this.props.subject) {
        case 'SCIENCE': {
          this.setState({ background: require('../assets/images/science/science.png') });

          break;
        }
        case 'ENGLISH': {
          this.setState({ background: require('../assets/images/english/english.png') });

          break;
        }
        case 'MATHS': {
          this.setState({ background: require('../assets/images/maths/maths.png') });

          break;
        }
        case 'ISLAMIC STUDIES': {
          this.setState({ background: require('../assets/images/islam/islam.png') });

          break;
        }
        case 'ARABIC': {
          this.setState({ background: require('../assets/images/arabic/arabic.png') });

          break;
        }
        case 'SOCIAL STUDIES': {
          this.setState({ background: require('../assets/images/sst/sst.png') });

          break;
        }
        default: return null;
      }
    }

    componentDidMount() {
      this.runTimer();
    }

    componentWillReceiveProps(nextProps) {
      console.log('Next Props: ', nextProps);
      this.setState({ question: nextProps.questions[Game.question_no] });
    }

    componentWillUpdate() {
     LayoutAnimation.easeInEaseOut();
   }
   componentWillUnmount() {
     this.stopTimer();
   }
    nextQuestion() {
      Game.question_no += 1;
      this.setState({
        question: this.props.questions[Game.question_no],
        answered: false,
        button1: require('../assets/images/common/1.png'),
        button2: require('../assets/images/common/1.png'),
        button3: require('../assets/images/common/1.png'),
        button4: require('../assets/images/common/1.png'),
        progress: 0
        });

        this.runTimer();
    }
    showCorrectORIncorrectButton(val, mybutton) {
      switch (val) {
        case 1: {
          this.setState({ button1: mybutton });
          break;
        }
        case 2: {
          this.setState({ button2: mybutton });
          break;
        }
        case 3: {
          this.setState({ button3: mybutton });
          break;
        }
        case 4: {
          this.setState({ button4: mybutton });
          break;
        }
        default: return null;
      }
    }
    showResult() {
      Game.question_no = 0;
      this.props.showResult(
        this.state.correctAnswers,
        this.props.questions.length,
        this.props.subject,
        this.props.player_id
      );
      if (this.props.noOfQuestions === this.state.correctAnswers) {
        console.log('character value', this.props.unlockCharacterNo);
        // this.setState({ currentUnlock: this.props.unlockCharacterNo + 1 });
        console.log(this.props.unlockCharacterNo);
        this.props.unlockCharacter(this.props.unlockCharacterNo + 1, this.props.player_id);
      } else {
        // this.props.showResult(
        //   this.state.correctAnswers,
        //   this.props.questions.length,
        //   this.props.subject,
        //   this.props.player_id
        // );
        this.props.navigation.navigate('Results');
      }
    }
    showResultButton() {
      if (this.props.noOfQuestions - 1 <= Game.question_no) {
        return (
          <TouchableOpacity
            onPress={() => {
              this.showResult();
            }}
           style={{ flex: 1,
              flexDirection: 'row-reverse',
              alignItems: 'center'
             }}
          >

            <Image
              style={{ height: 30, width: 30, resizeMode: 'contain' }}
              source={require('../assets/images/common/next.png')}
            />
            <Text style={[styles.nextText]}>Show Result </Text>
          </TouchableOpacity>
        );
      }
        return (
          <TouchableOpacity
            onPress={() => this.nextQuestion()}
           style={{ flex: 1,
              flexDirection: 'row-reverse',
              alignItems: 'center'
             }}
          >

            <Image
              style={{ height: 30, width: 30, resizeMode: 'contain' }}
              source={require('../assets/images/common/next.png')}
            />
            <Text style={[styles.nextText]}>Next Questions</Text>
          </TouchableOpacity>
        );
    }

    runTimer() {
      clearInterval(timer);
      timer = setInterval(() => {
        if (this.state.progress < 1) {
          this.setState({ progress: this.state.progress + 0.01 });
        } else {
          if (this.props.vibration) {
            Vibration.vibrate([0, 90, 90, 0]);
          }
          clearInterval(timer);
        }
      }, 100);
    }

    stopTimer() {
      clearInterval(timer);
    }
    checkAnswer(val) {
      this.setState({ answered: true });
      this.stopTimer();
      // console.log(val);
      // console.log('The current questionno.: ', Game.question_no);
      // Game.question_no += 1;
      // console.log(this.props.questions[Game.question_no]);
      // this.setState({ question: this.props.questions[Game.question_no], answered: true });
      if (val === this.state.question.answer) {
        if (this.props.music) {
          Sounds.playRightAnswer();
        }
        if (this.props.vibration) {
          Vibration.vibrate([0, 90, 90, 0]);
        }
        this.setState({
          remark2: 'Correct Answer!',
          correctAnswers: this.state.correctAnswers + 1
        });
        this.showCorrectORIncorrectButton(val, this.state.correct_answer_back);
      } else {
        if (this.props.music) {
          Sounds.playWrongAnswer();
        }
        if (this.props.vibration) {
          Vibration.vibrate([0, 90, 90, 0]);
        }
        this.setState({
          remark2: 'Wrong Answer!',
          incorrectAnswers: this.state.incorrectAnswers + 1,
          lives: this.state.lives - 1
        });
        this.showCorrectORIncorrectButton(this.state.question.answer,
           this.state.correct_answer_back);
           this.showCorrectORIncorrectButton(val, this.state.incorrect_answer_back);
      }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header title={'Header Text'} />
                <PlayerStats life={this.state.lives} spins={this.props.spinsCount} />
                <View style={[styles.bodyWidth]}>
                  <View
                    style={[{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        zIndex: this.state.progress >= 1 ? 2 : -2,
                        backgroundColor: 'rgba(0,0,0,0.65)',
                         height,
                         width
                    }]}
                  >
                      <TouchableOpacity
                         style={{ flex: 2 }}
                         onPress={() => this.nextQuestion()}
                      >
                        <Image
                          style={{ flex: 1,
                          height: undefined,
                          width: undefined,
                          resizeMode: 'contain' }}
                          source={require('../assets/images/timer.png')}
                        />
                      </TouchableOpacity>
                      <View style={{ flex: 1 }} />
                    </View>

                    <ProgressBarAndroid
                     color="red" progress={this.state.progress}
                     style={{ height: 3 }} styleAttr="Horizontal" indeterminate={false}
                    />
                    <Image
                     style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                      source={this.state.background}
                    >
                        <Image
                         style={styles.blackBoard}
                         source={require('../assets/images/common/board.png')}
                        >
                            <Text style={styles.headerText}> {this.props.subject} </Text>
                            <Text
                             style={styles.subHeading}
                            >{this.state.question.question}
                            </Text>
                        </Image>


                        {/*<View style={{ flex: 1, marginBottom: 20 }}>*/}
                            <TouchableOpacity
                             disabled={this.state.answered}
                              onPress={() => this.checkAnswer(1)}
                               style={styles.flexContainer}
                            >
                                <Image
                                style={styles.choiceBackground} source={this.state.button1}
                                >
                                    <Text
                                    style={styles.choice}
                                    >{this.state.question.option_a}
                                    </Text>
                                </Image>
                            </TouchableOpacity>
                            <TouchableOpacity
                             disabled={this.state.answered}
                              onPress={() => this.checkAnswer(2)}
                               style={styles.flexContainer}
                            >
                                <Image
                                 style={styles.choiceBackground}
                                  source={this.state.button2}
                                >
                                    <Text style={styles.choice}>
                                     {this.state.question.option_b}
                                    </Text>
                                </Image>
                            </TouchableOpacity>
                            <TouchableOpacity
                             disabled={this.state.answered}
                              onPress={() => this.checkAnswer(3)}
                               style={styles.flexContainer}
                            >
                                <Image
                                 style={styles.choiceBackground} source={this.state.button3}
                                >
                                    <Text
                                     style={styles.choice}
                                    >
                                    {this.state.question.option_c}
                                     </Text>
                                </Image>
                            </TouchableOpacity>
                            <TouchableOpacity
                             disabled={this.state.answered}
                              onPress={() => this.checkAnswer(4)}
                               style={styles.flexContainer}
                            >
                                <Image
                                 style={styles.choiceBackground}
                                  source={this.state.button4}
                                >
                                  <Text style={styles.choice}>
                                     {this.state.question.option_d}
                                  </Text>
                                </Image>
                            </TouchableOpacity>
                        {/*</View>*/}
                        <Image
                        source={this.state.teacher}
                          style={{ height: 100,
                             width: 100,
                             left: 0,
                             position: 'absolute',
                             resizeMode: 'contain',
                             zIndex: 2,
                             opacity: this.state.answered ? 1 : 0 }}
                        />
                        <View
                          style={{ flex: 2, justifyContent: 'flex-end', opacity: this.state.answered ? 1 : 0 }}
                        >

                            <View
                             style={{
                              backgroundColor: 'rgba(0,0,0,0.65)',
                              justifyContent: 'space-between',
                              flexDirection: 'row',
                              alignItems: 'center',
                              paddingHorizontal: 10,
                              height: 50
                             }}
                            >
                            <View style={{ flex: 1 }}>
                              <Text style={[styles.remark1, { color: 'orange' }]}>
                                {this.state.remark2}
                              </Text>
                            </View>
                           { this.showResultButton() }
                          </View>
                        </View>
                    </Image>
                </View>
             <DialogBox ref={(dialogbox) => { this.dialogbox = dialogbox; }} />
          </View>
        );
    }
}
const styles = StyleSheet.create({
    questionsAsked: {
        flex: 1,
        alignItems: 'center',
    },
    fullWidth: {
        flex: 1,
        backgroundColor: 'white'
    },
    nextText: {
        fontFamily: 'mikadoultra',
        fontSize: 16,
        color: '#fff'
    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        zIndex: 1000
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 16
    },
    subHeading: {
        fontSize: 16,
        marginLeft: 50,
        marginRight: 50,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        position: 'absolute',
        top: 60,
        left: 290,
        right: 290
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        alignItems: 'center',
        // marginBottom: 10,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50,
    },
    bodyWidth: {
        flex: 19
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    blackBoard: {
        flex: 8,
        resizeMode: 'contain',
        alignSelf: 'center',
        margin: 20,
    },
    boardContainer: {
        flex: 18,

    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontFamily: 'mikadoultra',
        textAlign: 'center',
        marginTop: 20,
        textDecorationLine: 'underline',
    },
    remark1: {
      fontSize: 22,
      fontFamily: 'mikadoultra'
    },
    choiceBoard: {
        flex: 1,
    },
    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
});

const mapStateToProps = (state) => ({
    questions: state.gamequestion.questions,
    noOfQuestions: state.gamequestion.questions.length,
    subject: state.gamequestion.subject,
    spinsCount: state.gamequestion.spinsCount,
    music: state.settings.music,
    unlockCharacterNo: state.gamequestion.unlockCharacterList.length,
    vibration: state.settings.vibration,
    player_id: state.auth.player.player_id
  });
export default connect(mapStateToProps, { questionFetch, showResult, unlockCharacter, fetchUnlockCharacters })(Subject);
