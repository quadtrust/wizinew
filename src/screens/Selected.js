import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
// import Header from  './Header'
// import API from './API'
// import language from './Language'
import Header from '../components/Header';
import { questionFetch } from '../actions';

// const id = '';


class Selected extends Component {

    constructor(props) {
        super(props);
        this.state = {
            image: require('../assets/images/common/arabic-character.png'),
            isBigText: false
        };
    }


    componentWillMount() {
      if (this.props.subject.length > 13) {
        this.setState({ isBigText: true });
      }
      switch (this.props.subject) {
        case 'SCIENCE': {
          this.setState({ image: require('../assets/images/common/science-character.png') });

          break;
        }

        case 'ENGLISH': {
          this.setState({ image: require('../assets/images/common/english-character.png') });

          break;
        }
        case 'MATHS': {
          this.setState({ image: require('../assets/images/common/maths-character.png') });

          break;
        }
        case 'ISLAMIC STUDIES': {
          this.setState({ image: require('../assets/images/common/islamic-character.png') });

          break;
        }
        case 'ARABIC': {
          this.setState({ image: require('../assets/images/common/arabic-character.png') });

          break;
        }
        case 'SOCIAL STUDIES': {
          this.setState({ image: require('../assets/images/common/social-character.png') });

          break;
        }
        default: return null;
      }
    }

    takeTest() {
      this.props.navigation.navigate('Subject');
    }

    spinAgain() {
      if (this.props.spinsCount === 0) {
        console.log('Cannot Spin no spins');
      } else {
        this.props.navigation.navigate('Spinner');
      }
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
              <Header title={'Subject'} />
                <View style={[styles.bodyWidth]}>
                    <View style={[this.state.isBigText ? { flex: 0.4 } : { flex: 0.7 }, { justifyContent: 'center' }]}>
                        <Text
                            style={[this.state.isBigText ?
                              styles.bigFont : styles.heading, styles.blue, styles.textCenter, {}]}
                        >
                            {this.props.subject}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3 }}>
                        <Text
                        style={[styles.bold, styles.black, styles.textCenter, styles.mediumFont]}
                        >
                            HAS BEEN SELECTED
                        </Text>
                    </View>
                    <View style={[{ flex: 2 }]}>
                        <Image
                        style={[styles.contain,
                           { height: null, width: null, flex: 2 }]}
                           source={this.state.image}
                        />

                        <Text style={[styles.textCenter, styles.orange, styles.bold, styles.h1]}>
                            YOU HAVE 3 OPTIONS
                        </Text>
                    </View>
                    <View style={{ flex: 0.8, flexDirection: 'row' }}>

                        <TouchableOpacity
                        style={[{
                            flex: 1,
                             borderRightWidth: 1,
                            borderRightColor: '#555253',
                            marginTop: 30,
                            marginBottom: 10
                        }]}
                        onPress={() => this.takeTest()}
                        >
                            <Image
                            source={require('../assets/images/spin_selection/test.png')}
                            style={{ height: null, width: null, resizeMode: 'contain', flex: 1 }}
                            />
                            <Text
                                style={{
                                flex: 1,
                                color: 'black',
                                textAlign: 'center',
                                fontFamily: 'mikadoultra'
                            }}
                            >TAKE THE TEST</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        style={[{
                            flex: 1,
                            borderRightWidth: 1,
                            borderRightColor: '#555253',
                            marginTop: 30,
                            marginBottom: 10
                        }]}
                         onPress={() => this.spinAgain()}
                        >
                            <Image
                            source={require('../assets/images/spin_selection/respin.png')}
                            style={{ height: null, width: null, resizeMode: 'contain', flex: 1 }}
                            />
                            <Text
                            style={{ flex: 1,
                               color: 'black',
                             textAlign: 'center',
                              fontFamily: 'mikadoultra' }}
                            >
                                SPIN AGAIN
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        style={[{ flex: 1, marginTop: 30, marginBottom: 10 }]}
                          onPress={() => {
                              this.props.navigation.navigate('SubjectSelection');
                          }}
                        >
                            <Image
                            source={require('../assets/images/spin_selection/subject.png')}
                            style={{ height: null, width: null, resizeMode: 'contain', flex: 1 }}
                            />
                            <Text
                            style={{ flex: 1,
                              color: 'black',
                              textAlign: 'center',
                              fontFamily: 'mikadoultra' }}
                            >
                                SUBJECT SELECTION</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: 'white'
    },
    heading: {
        fontSize: 70,
        fontFamily: 'mikadoultra',
    },
    bigFont: {
        fontSize: 40,
        fontFamily: 'mikadoultra',
    },
    h1: {
        fontSize: 25
    },
    black: {
        color: 'black'
    },
    mediumFont: {
        fontSize: 20
    },
    orange: {
        color: '#FF9D00'
    },
    blue: {
        color: '#4B82C3'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    bodyWidth: {
        flex: 18
    },
    bold: {
        fontFamily: 'mikadoultra'
    },

    zoomed: {
        resizeMode: 'cover'
    },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
});

const mapStateToProps = (state) => {
  return {
    subject: state.gamequestion.subject,
    spinsCount: state.gamequestion.spinsCount
  };
};

export default connect(mapStateToProps, { questionFetch })(Selected);
