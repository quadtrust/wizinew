import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Animated
} from 'react-native';

const TIMES = 400;
let rotation;
let last_spin = 0;
export default class RandomSpinner extends Component {
    constructor(props) {
        super(props);

        this.state = {
            subjectRotation: new Animated.Value(0),
            user2_id: null,
            spinning: false
        };
    }

    _spin() {
        if (!this.state.spinning) {
            this.setState({ spinning: true });
            let subjects_rotate = Math.floor(Math.random() * 360) + 1;
            if (
                (subjects_rotate >= 31 && subjects_rotate <= 36) ||
                (subjects_rotate >= 91 && subjects_rotate <= 96) ||
                (subjects_rotate >= 149 && subjects_rotate <= 155) ||
                (subjects_rotate >= 210 && subjects_rotate <= 216) ||
                (subjects_rotate >= 269 && subjects_rotate <= 275) ||
                (subjects_rotate >= 329 && subjects_rotate <= 335)
            ) {
                subjects_rotate += 6
            }
            subjects_rotate = subjects_rotate + (3 * 360);
            subjects_rotate += last_spin;
            last_spin = subjects_rotate;

            // this.props.reduceSpin();
            rotation = '';
            // this.state.subjectRotation.addListener((value) => {
            //     rotation = JSON.stringify(value.value) % 360;
            // });
            Animated.timing(
                this.state.subjectRotation,
                {
                    toValue: subjects_rotate,
                    duration: 3000
                }
            ).start((resp) => {
                if (resp.finished) {
                    this.setState({ spinning: false });
                    let id = 0,
                    theme = '';
                    if (rotation > 36 && rotation <= 91) {
                        id = 2;
                        theme = 'maths';
                        // AsyncStorage.setItem('theme', 'maths');
                    } else if (rotation > 96 && rotation <= 149) {
                        id = 6;
                        theme = 'social';
                        // AsyncStorage.setItem('theme', 'social');
                    } else if (rotation > 155 && rotation <= 210) {
                        id = 3;
                        theme = 'islam';
                        // AsyncStorage.setItem('theme', 'islam');
                    } else if (rotation > 216 && rotation <= 269) {
                        id = 5;
                        theme = 'science';
                        // AsyncStorage.setItem('theme', 'science');
                    } else if (rotation > 275 && rotation <= 329) {
                        id = 1;
                        theme = 'english';
                        // AsyncStorage.setItem('theme', 'english');
                    } else {
                        id = 4;
                        theme = 'arabic';
                        // AsyncStorage.setItem('theme', 'arabic');
                    }
                }
            });
        }
    }

    render() {
        const spin = this.state.subjectRotation.interpolate({
            inputRange: [0, 360],
            outputRange: ['0deg', '360deg']
        });
        return (
            <View style={styles.fullWidth}>
                <View style={[styles.bodyWidth, styles.whiteBackground]}>
                    <Text style={styles.mainHeading}>SPIN THE WHEEL</Text>
                    <View style={{ flex: 3 }}>
                        <Animated.Image
                            source={require('../assets/images/spinner/subjects.png')}
                            style={{
                                flex: 1,
                                height: undefined,
                                width: undefined,
                                resizeMode: 'contain',
                                margin: 26,
                                zIndex: -1,
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0,
                                transform: [{
                                    rotate: spin
                                }]
                            }}
                        />

                        <Image
                            source={require('../assets/images/spinner/circumference.png')}
                            style={{
                                flex: 1,
                                height: undefined,
                                width: undefined,
                                resizeMode: 'contain',
                                margin: 15,
                                zIndex: -1,
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            }}
                        />


                        <TouchableOpacity
                            style={{ flex: 1, margin: 125, zIndex: 1 }} onPress={() => this._spin()}
                        >
                            <Image
                                source={require('../assets/images/spinner/center.png')}
                                style={styles.centerImage}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1
    },
    bodyWidth: {
        flex: 18
    },
    whiteBackground: {
        backgroundColor: '#2B292A'
    },
    mainHeading: {
      textAlign: 'center',
      fontSize: 30,
      fontWeight: 'bold',
      color: 'white',
      padding: 20
    },
    centerImage: {
      flex: 1,
      height: undefined,
      width: undefined,
      resizeMode: 'contain'
    }
});
