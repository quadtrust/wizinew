import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native';
import Header from '../components/Header';

export default class Statistics extends Component {
    render() {
      return (
        <View style={styles.fullWidth}>
          <View style={styles.bodyWidth}>
            <Header title={'STATISICS'} />
            <Text style={[styles.heading]}>STATISICS</Text>
              <ScrollView>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                        style={[styles.iconSize]}
                        source={require('../assets/images/common/hat.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>TEST ATTEMPTED</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                    <Text style={[styles.score]}>7</Text>
                  </View>
                </View>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                        style={[styles.iconSize]}
                        source={require('../assets/images/common/trophy.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>CORRECT ANSWERS</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                    <Text style={[styles.score]}>12</Text>
                  </View>
                </View>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                      style={[styles.iconSize]}
                      source={require('../assets/images/common/heart.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>LIVES LEFT</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                    <Text style={[styles.score]}>3</Text>
                  </View>
                </View>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                      style={[styles.iconSize]}
                      source={require('../assets/images/common/spin.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>SPINS LEFT</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                    <Text style={[styles.score]}>5</Text>
                  </View>
                </View>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                        style={[styles.iconSize]}
                        source={require('../assets/images/common/coin.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>WIZI COINS</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                      <Text style={[styles.score]}>8</Text>
                  </View>
                </View>
                <View style={[styles.statsContainer]}>
                  <View style={[styles.iconView]}>
                    <Image
                        style={[styles.iconSize]}
                        source={require('../assets/images/common/invite.png')}
                    />
                  </View>
                  <View style={[styles.attempView]}>
                    <Text style={[styles.attemp]}>NUMBER OF INVITES</Text>
                  </View>
                  <View style={[styles.scoreView]}>
                    <Text style={[styles.score]}>0</Text>
                  </View>
                </View>
              </ScrollView>
          </View>
        </View>
      );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    statsContainer: {
        height: 70,
        marginTop: 15,
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 8,
        borderBottomWidth: 2,
        borderColor: '#7A7B7C'
    },
    scoreView: { flex: 2, justifyContent: 'center' },
    attempView: {
      flex: 4, 
      justifyContent: 'center',
      borderLeftWidth: 1,
      borderColor: '#7D7E7F'
    },
    iconView: { flex: 2, justifyContent: 'center', alignItems: 'center' },
    iconSize: { height: 60, width: 70, resizeMode: 'contain' },
    attemp: {
      textAlign: 'left',
      marginLeft: 12,
      color: '#000102',
      fontSize: 18,
      fontFamily: 'mikadoultra'
     },
    score: {
      textAlign: 'center',
      fontSize: 25,
      fontFamily: 'mikadoultra',
      color: '#6ADC97'
    },
    heading: {
      fontSize: 28,
      fontFamily: 'mikadoultra',
      color: '#2895D0',
      textAlign: 'center',
      marginTop: 10
    },

    bodyWidth: {
      flex: 18
    }
});
