import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';

export default class Competition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            char1: 'Bhavya',
            char2: 'Neeraj',
            char3: 'Sankalp',
            char4: 'Tousif',
            char5: 'Teja',
            char6: 'Prithvi'
        };
    }
    render() {
      return (
        <View style={[styles.fullWidth]}>
          <View style={[styles.bodyWidth, styles.whiteBack, { alignItems: 'center' }]}>
              <Text style={[styles.center, styles.competitionText]}>
                {'Competition'.toUpperCase()}
              </Text>
              <Image
                source={require('../assets/images/map.png')}
                style={{ height: 250, width: 320, resizeMode: 'contain' }}
              />
              <View
                style={[styles.row,
                styles.headingContainer]}
              >
                  <View style={{ flex: 1 }}>
                      <Text style={styles.headingText}>GOVERNORATE</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.headingText, { textAlign: 'center' }]}>PLAYERS</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.headingText, { textAlign: 'right' }]}>SCORES</Text>
                  </View>

              </View>
              <View style={[styles.row, styles.headingContainer]}>
                  <View style={{ flex: 1 }}>
                      <Text style={styles.scoreText}>{this.state.char5}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'center' }]}>2</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'right' }]}>1200</Text>
                  </View>

              </View>
              <View style={[styles.row, styles.headingContainer]}>
                  <View style={{ flex: 1 }}>
                      <Text style={styles.scoreText}>{this.state.char1}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'center' }]}>2</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={[styles.scoreText, { textAlign: 'right' }]}>1200</Text>
                  </View>
              </View>
              <View style={[styles.row, styles.headingContainer]}>
                  <View style={{ flex: 1 }}>
                      <Text style={styles.scoreText}>{this.state.char2}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'center' }]}>2</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'right' }]}>1200</Text>
                  </View>
              </View>
              <View style={[styles.row, styles.headingContainer]}>
                  <View style={{ flex: 1 }}>
                      <Text style={styles.scoreText}>{this.state.char3}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'center' }]}>2</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'right' }]}>1200</Text>
                  </View>
              </View>
              <View style={[styles.row, styles.headingContainer]}>
                  <View style={{ flex: 1 }}>
                      <Text style={styles.scoreText}>{this.state.char4}</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'center' }]}>2</Text>
                  </View>
                  <View style={{ flex: 1 }}>
                      <Text style={[styles.scoreText, { textAlign: 'right' }]}>1200</Text>
                  </View>
              </View>
          </View>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
      flex: 1,
      backgroundColor: '#fff'
  },
  bodyWidth: {
    flex: 18
},
whiteBack: {
    backgroundColor: '#FEFFFF'
},
headingText: {
  color: '#AA0922',
  fontSize: 15,
  fontWeight: '700'
},
center: {
    textAlign: 'center'
},
headingContainer: {
  justifyContent: 'space-around',
  marginHorizontal: 10,
  borderBottomWidth: 1,
  borderColor: 'black'
},
  competitionText: {
    color: '#139BFF',
    fontWeight: '900',
    fontSize: 30,
  },
  row: {
    flexDirection: 'row'
  },
  scoreText: {
    color: '#159CFF',
    fontSize: 15,
    fontWeight: '700'
  },
});
