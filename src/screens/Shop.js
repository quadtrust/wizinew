import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';
import Header from '../components/Header';

export default class Shop extends Component {
  render() {
    return (
      <View style={styles.fullWidth}>
        <Header title={'WIZI QUIZ'} />
        <View style={styles.bodyWidth}>
          <Text style={[styles.heading, styles.backColor]}>SHOP</Text>
          <ScrollView>
            <View style={[styles.statsContainer]}>
              <View style={[styles.iconView]}>
                <Image
                  style={[styles.iconSize]}
                  source={require('../assets/images/common/heart.png')}
                />
              </View>
              <View style={[styles.attempView]}>
                <Text style={[styles.attemp]}>10 LIVES</Text>
              </View>
              <TouchableOpacity style={[styles.buttonBody]}>
                <Text style={[styles.textCenter, styles.buttonText]}>
                  KWD 3.00
                </Text>
              </TouchableOpacity>

            </View>
            <View style={[styles.statsContainer]}>
              <View style={[styles.iconView]}>
                <Image
                  style={[styles.iconSize]}
                  source={require('../assets/images/common/spin.png')}
                />
              </View>
              <View style={[styles.attempView]}>
                <Text style={[styles.attemp]}>SPINS</Text>
              </View>
              <TouchableOpacity style={[styles.buttonBody]}>
                <Text style={[styles.textCenter, styles.buttonText]}>
                  KWD 7.15
                </Text>
              </TouchableOpacity>

            </View>
            <View style={[styles.statsContainer]}>
              <View style={[styles.iconView]}>
                <Image
                    style={[styles.iconSize]}
                    source={require('../assets/images/common/coin.png')}
                />
              </View>
              <View style={[styles.attempView]}>
                <Text style={[styles.attemp]}>WIZI COINS</Text>
              </View>
              <TouchableOpacity style={[styles.buttonBody]}>
                <Text style={[styles.textCenter, styles.buttonText]}>
                  KWD 4.00
                </Text>
              </TouchableOpacity>

            </View>
            <View style={[styles.statsContainer]}>
              <View style={[styles.iconView]}>
                <Image
                  style={[styles.iconSize]}
                  source={require('../assets/images/common/premium.png')}
                />
              </View>
              <View style={[styles.attempView]}>
                <Text style={[styles.attemp]}>PREMIUM VERSION</Text>
              </View>
              <TouchableOpacity style={[styles.buttonBody]}>
                <Text style={[styles.textCenter, styles.buttonText]}>
                  KWD 9.00
                </Text>
              </TouchableOpacity>

            </View>
            <View style={[styles.statsContainer]}>
              <View style={[styles.iconView]}>
                <Image
                    style={[styles.iconSize]}
                    source={require('../assets/images/common/time.png')}
                />
              </View>
              <View style={[styles.attempView]}>
                <Text style={[styles.attemp]}>EXTRA TIME</Text>
              </View>
              <TouchableOpacity style={[styles.buttonBody]}>
                <Text style={[styles.textCenter, styles.buttonText]}>
                  KWD 2.50
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    fullWidth: {
      flex: 1,
      backgroundColor: '#fff'
    },
    backColor: {
      backgroundColor: '#1EBBA7'
    },
    statsContainer: {
      height: 70,
      marginTop: 15,
      flex: 1,
      flexDirection: 'row',
      paddingBottom: 8,
      borderBottomWidth: 2,
      borderColor: '#7A7B7C',
    },
    scoreView: { flex: 2, justifyContent: 'center' },
    attempView: {
      flex: 4,
      justifyContent: 'center',
      borderLeftWidth: 1,
      borderColor: '#7D7E7F',
    },
    iconView: { flex: 2, justifyContent: 'center', alignItems: 'center' },
    iconSize: { height: 60, width: 70, resizeMode: 'contain' },
    attemp: {
      textAlign: 'left',
      marginLeft: 7,
      color: '#000102',
      fontSize: 18,
      fontFamily: 'mikadoultra',
    },
    heading: {
      fontSize: 30,
      fontFamily: 'mikadoultra',
      color: '#fff',
      textAlign: 'center'
    },
    buttonBody: {
      backgroundColor: '#1EBBA7',
      width: 80,
      height: 30,
      marginTop: 12,
      borderRadius: 5,
      marginRight: 10,
    },
    buttonText: {
      color: 'white',
      marginTop: 5,
      fontSize: 13,
      fontFamily: 'mikadoultra',
      textAlign: 'center'
    },
    bodyWidth: {
      flex: 18
    }
});
