import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import Header from '../components/Header';

 class ChooseLanguage extends Component {
    constructor(props) {
      super(props);

      this.state = {
        english: '',
        arabic: '',
        englishStyle: {},
        arabicStyle: {}
      };
    }

    selectEnglish() {
      this.setState({
        englishStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
        arabicStyle: {},
        english: true,
        arabic: false
      });
    }

    selectArabic() {
      this.setState({
        arabicStyle: { backgroundColor: 'rgba(0, 0, 0, 0.6)' },
        englishStyle: {},
        arabic: true,
        english: false
      });
    }

    // changeLanguage() {
    //   let language = null;
    //   if (this.state.english) {
    //     language = 'ENGLISH';
    //   } else if (this.state.arabic) {
    //     language = 'ARABIC';
    //   }
    // }

    render() {
      return (
          <View style={[styles.fullWidth]}>
            <Header title={'WIZI QUIZE'} />
            <View style={[styles.bodyWidth, styles.whiteBack]}>
              <Image
                  style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                  source={require('../assets/images/language/back.png')}
              >
                <View style={[styles.bodyWidth, { justifyContent: 'center' }]} >
                  <View>
                    <Text style={[styles.languageHeader]}>
                      CHOOSE YOUR LANGUAGE
                    </Text>
                  </View>
                  <View style={[styles.row, { justifyContent: 'space-around', marginTop: 30, marginBottom: 20 }]}>
                    <View style={[{ padding: 5 }, this.state.englishStyle]}>
                      <TouchableOpacity onPress={() => this.selectEnglish()}>
                        <Image
                          style={styles.englishImage}
                          source={require('../assets/images/language/english.png')}
                        />
                        <Text style={styles.languageText}>ENGLISH</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={[{ padding: 5 }, this.state.arabicStyle]}>
                      <TouchableOpacity onPress={() => this.selectArabic()}>
                        <Image
                          style={styles.englishImage}
                          source={require('../assets/images/language/arabic.png')}
                        />
                        <Text style={styles.languageText}>ARABIC</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{ alignItems: 'center' }}>
                    <View
                     style={[{
                        marginTop: 20,
                        padding: 7,
                        width: 120,
                        borderRadius: 5,
                        backgroundColor: '#EC0005',
                    }]}
                    >
                      <TouchableOpacity>
                        <Text style={styles.buttonText}>CHANGE</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Image>
            </View>
          </View>
        );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1,
    backgroundColor: '#fff'
  },
  white: {
    color: 'white'
  },
  bodyWidth: {
    flex: 18
  },
  whiteBack: {
    backgroundColor: '#FEFFFF'
  },

  row: {
    flexDirection: 'row'
  },
  zoomed: {
    resizeMode: 'cover'
  },
  languageHeader: {
    fontFamily: 'mikadoultra',
    fontSize: 23,
    color: 'white',
    textAlign: 'center'
  },
  notKnown: {
    height: undefined,
    width: undefined
  },
  buttonText: {
    fontSize: 18, color: 'white', fontFamily: 'mikadoultra', textAlign: 'center'
  },
  englishImage: {
    resizeMode: 'contain',
    height: 80,
    width: 80
  },
  languageText: {
    fontFamily: 'mikadoultra', color: 'white', fontSize: 20
  }
});

export default ChooseLanguage;
