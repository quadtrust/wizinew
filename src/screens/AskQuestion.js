import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    ToastAndroid,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import Header from '../components/Header';

import { submitQuestion } from '../actions/';

 class AskQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',
            choice1: '',
            choice2: '',
            choice3: '',
            choice4: '',
            remark: '',
            answer_remark: '',
            teacher: require('../assets/images/common/monster.png'),
            background: require('../assets/images/islam/islam.png'),
            correct_choice_back: require('../assets/images/common/2.png'),
            editing: true,
            choice_1_back: require('../assets/images/common/1.png'),
            choice_2_back: require('../assets/images/common/1.png'),
            choice_3_back: require('../assets/images/common/1.png'),
            choice_4_back: require('../assets/images/common/1.png'),
            correct_choice: 0,
            ClassId: 0,
            CategoryId: 0
        };

        this.nextOrSubmit = this.nextOrSubmit.bind(this);
    }
    nextOrSubmit() {
      if (this.state.editing) {
        return (
          <TouchableOpacity
            style={{ flexDirection: 'row' }} onPress={() => this.nextStep()}
          >
            <Text style={[styles.nextText]}>NEXT STEP</Text>
            <Image
              style={{ height: 30, width: 30, resizeMode: 'contain' }}
              source={require('../assets/images/common/next.png')}
            />
          </TouchableOpacity>
        );
      }
        return (
          <TouchableOpacity
            style={{ flexDirection: 'row' }} onPress={() => this.nextStep()}
          >
            <Text style={[styles.nextText]}>SUBMIT</Text>
            <Image
              style={{ height: 30, width: 30, resizeMode: 'contain' }}
              source={require('../assets/images/common/next.png')}
            />
          </TouchableOpacity>
        );
    }

    nextStep() {
        if (
            this.state.question.length > 0 &&
            this.state.choice1.length > 0 &&
            this.state.choice2.length > 0 &&
            this.state.choice3.length > 0 &&
            this.state.choice4.length > 0 &&
            this.state.editing
        ) {
            this.setState({
                editing: !this.state.editing
            });
            ToastAndroid.show('Select correct choice', ToastAndroid.SHORT);
        } else if (!this.state.editing) {
            if ([1, 2, 3, 4].indexOf(this.state.correct_choice) >= 0) {
              const question = {};
              question.subject = this.props.subject;
              question.question = this.state.question;
              question.option_a = this.state.choice1;
              question.option_b = this.state.choice2;
              question.option_c = this.state.choice3;
              question.option_d = this.state.choice4;
              question.answer = this.state.correct_choice;
                this.props.submitQuestion(question);
            } else {
                ToastAndroid.show('Select an answer first', ToastAndroid.SHORT);
            }
        } else {
            ToastAndroid.show('Fill all the values', ToastAndroid.SHORT);
        }
    }

    chooseAnswer(number) {
        if (!this.state.editing) {
            if (number === 1) {
                this.setState({
                    choice_1_back: this.state.correct_choice_back,
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 1
                });
            } else if (number === 2) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: this.state.correct_choice_back,
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 2
                });
            } else if (number === 3) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: this.state.correct_choice_back,
                    choice_4_back: require('../assets/images/common/1.png'),
                    correct_choice: 3
                });
            } else if (number === 4) {
                this.setState({
                    choice_1_back: require('../assets/images/common/1.png'),
                    choice_2_back: require('../assets/images/common/1.png'),
                    choice_3_back: require('../assets/images/common/1.png'),
                    choice_4_back: this.state.correct_choice_back,
                    correct_choice: 4
                });
            } else {
                ToastAndroid.show('Select correct choice', ToastAndroid.SHORT);
            }
        }
    }

    render() {
        return (
            <View style={[styles.fullWidth, { opacity: this.state.fade }]} accessible={false}>
                <Header title={'WIZI QUIZ'} />
                <View style={[styles.bodyWidth]}>
                    <Image
                        style={[styles.fullWidth, styles.zoomed, styles.notKnown]}
                        source={this.state.background}
                    >
                        <Image
                            style={[styles.blackBoard]}
                            source={require('../assets/images/common/board.png')}
                        >
                            <Text style={styles.headerText}> {this.props.subject}</Text>
                            { this.state.editing ? <TextInput
                                style={styles.questionInput}
                                placeholder={'Write your question here'}
                                placeholderTextColor={'white'}
                                onChangeText={question => this.setState({ question })}
                                value={this.state.question}
                                multiline
                            /> : <Text
                                  style={{
                                  color: 'white',
                                  fontSize: 18,
                                  textAlign: 'center',
                                  margin: 15
                                  }}
                            >{this.state.question}</Text>
                              }
                        </Image>
                        <TouchableOpacity
                          style={styles.flexContainer}
                          onPress={() => this.chooseAnswer(1)}
                        >
                            <Image
                              style={styles.choiceBackground}
                              source={this.state.choice_1_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={styles.choiceInput}
                                    placeholder={'Choice 1'}
                                    onChangeText={choice1 => this.setState({ choice1 })}
                                    value={this.state.choice1}
                                /> : <Text style={styles.choice}>{this.state.choice1}</Text> }

                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.flexContainer}
                          onPress={() => this.chooseAnswer(2)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_2_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={styles.choiceInput}
                                    placeholder={'Choice 2'}
                                    onChangeText={choice2 => this.setState({ choice2 })}
                                    value={this.state.choice2}
                                /> : <Text style={styles.choice}> {this.state.choice2} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.flexContainer}
                          onPress={() => this.chooseAnswer(3)}
                        >
                            <Image
                              style={styles.choiceBackground}
                              source={this.state.choice_3_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={styles.choiceInput}
                                    placeholder={'Choice 3'}
                                    onChangeText={choice3 => this.setState({ choice3 })}
                                    value={this.state.choice3}
                                /> : <Text style={styles.choice}> {this.state.choice3} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.flexContainer}
                          onPress={() => this.chooseAnswer(4)}
                        >
                            <Image
                                style={styles.choiceBackground}
                                source={this.state.choice_4_back}
                            >
                                { this.state.editing ? <TextInput
                                    style={styles.choiceInput}
                                    placeholder={'Choice 4'}
                                    onChangeText={choice4 => this.setState({ choice4 })}
                                    value={this.state.choice4}
                                /> : <Text style={styles.choice}> {this.state.choice4} </Text> }
                            </Image>
                        </TouchableOpacity>
                        <View style={{ position: 'absolute', left: 10 }}>
                            <Image
                              source={this.state.teacher}
                               style={{
                                   resizeMode: 'contain',
                                   height: 120,
                                   width: 130,
                                   zIndex: 9999
                               }}
                            />
                        </View>
                        <View style={styles.nextContainer}>
                          {this.nextOrSubmit()}
                      </View>
                    </Image>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1,
    backgroundColor: '#fff'
  },
  bodyWidth: {
    flex: 18
},
zoomed: {
      resizeMode: 'cover'
  },
  notKnown: {
      height: undefined,
      width: undefined
  },
  blackBoard: {
    height: null,
    width: null,
    flex: 4,
    resizeMode: 'stretch',
    marginTop: 20,
    marginHorizontal: 20
},
headerText: {
    fontSize: 25,
    color: '#fff',
    fontFamily: 'mikadoultra',
    textAlign: 'center',
    marginTop: 30,
    textDecorationLine: 'underline',
},
flexContainer: {
    flex: 1,
    marginLeft: 50,
    marginRight: 50
},
nextContainer: {
    backgroundColor: 'rgba(0,0,0,0.65)',
    flexDirection: 'row-reverse',
    marginTop: 60,
    padding: 10
  },
choiceBackground: {
    height: undefined,
    width: undefined,
    flex: 1,
    resizeMode: 'contain',
    paddingHorizontal: 35,
    paddingVertical: 5,
    justifyContent: 'center'
},
choiceInput: {
  flex: 1,
  textAlign: 'center',
  fontFamily: 'mikadoultra',
  fontSize: 16
},
questionInput: {
    flex: 1,
    margin: 15,
    textAlign: 'center',
    justifyContent: 'center',
    color: 'white'
},
choice: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'mikadoultra',
    fontSize: 15,
    paddingTop: 2
},
nextText: {
    fontFamily: 'mikadoultra',
    fontSize: 20,
    color: '#fff'
},
});
function mapStateToProps(state) {
  return {
    subject: state.Questions.subject
  };
}

export default connect(mapStateToProps, { submitQuestion })(AskQuestion);
