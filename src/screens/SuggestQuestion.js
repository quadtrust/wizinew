import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { addSubjectForQuestion } from '../actions/';
import Header from '../components/Header';


class SuggestQuestion extends Component {
    render() {
        return (
            <View style={[styles.fullWidth]}>
                <Header title={'WIZI QUIZ'} />
                <View style={[styles.bodyWidth, styles.whiteBack]}>
                    <View style={[{ backgroundColor: '#21AF96' }]}>
                        <Text style={[styles.center, styles.suggestQuestionText]}>
                            SUGGEST QUESTION
                        </Text>
                    </View>
                    <View>
                        <Text style={[styles.center, styles.selectCatText]}>
                            SELECT YOUR CATEGORY
                        </Text>
                    </View>
                    <View style={[{ backgroundColor: '#3C434C' }]}>
                        <Text style={[styles.center, styles.fewerText]}>
                          CATEGORY WITH FEWER QUESTIONS
                        </Text>
                    </View>
                    <View
                      style={[styles.row, styles.subjectListWrapper]}
                    >
                        <View style={styles.subjectAvatarContainer}>
                            <TouchableOpacity
                              onPress={() => this.props.addSubjectForQuestion('maths')}
                              style={{ flex: 1 }}
                            >
                                <Image
                                   style={styles.avatarStyle}
                                   source={require('../assets/images/common/maths-character.png')}
                                />
                                <Text
                                    style={[styles.center, styles.subjectText]}
                                >MATHS</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={styles.subjectAvatarContainer}
                        >
                            <TouchableOpacity
                              onPress={() => this.props.addSubjectForQuestion('Islamic Studies')}
                              style={{ flex: 1 }}
                            >
                              <Image
                                style={styles.avatarStyle}
                                source={require('../assets/images/common/islamic-character.png')}
                              />
                              <Text style={[styles.center, styles.subjectText]}>
                                ISLAMIC STUDIES
                              </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, padding: 15 }}>
                          <TouchableOpacity
                            onPress={() => this.props.addSubjectForQuestion('arabic')}
                            style={{ flex: 1 }}
                          >
                            <Image
                              style={styles.avatarStyle}
                              source={require('../assets/images/common/arabic-character.png')}
                            />
                            <Text style={[styles.center, styles.subjectText]}>ARABIC</Text>
                          </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[{ backgroundColor: '#3C434C', marginTop: 1, marginBottom: 1 }]}>
                      <Text
                        style={[styles.center, { color: 'white', fontSize: 18, fontFamily: 'mikadoultra' }]}
                      >OTHER CATEGORY</Text>
                    </View>
                    <View
                      style={[styles.row, styles.subjectListWrapper]}
                    >
                        <View
                          style={styles.subjectAvatarContainer}
                        >
                            <TouchableOpacity
                              onPress={() => this.props.addSubjectForQuestion('science')}
                                style={{ flex: 1 }}
                            >
                                <Image
                                  style={styles.avatarStyle}
                                  source={require('../assets/images/common/science-character.png')}
                                />
                                <Text
                                  style={[styles.center, styles.subjectText]}
                                >SCIENCE</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.subjectAvatarContainer}>
                            <TouchableOpacity
                              onPress={() => this.props.addSubjectForQuestion('Social Studies')}
                              style={{ flex: 1 }}
                            >
                                <Image
                                  style={styles.avatarStyle}
                                  source={require('../assets/images/common/social-character.png')}
                                />
                                <Text
                                  style={[styles.center, styles.subjectText]}
                                >SOCIAL STUDIES</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, padding: 15 }}>
                            <TouchableOpacity
                              onPress={() => this.props.addSubjectForQuestion('english')}
                              style={{ flex: 1 }}
                            >
                              <Image
                                style={styles.avatarStyle}
                                source={require('../assets/images/common/english-character.png')}
                              />
                                <Text
                                  style={[styles.center, styles.subjectText]}
                                >ENGLISH</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    bodyWidth: {
      flex: 18
  },
  whiteBack: {
    backgroundColor: '#FEFFFF'
},
suggestQuestionText: {
  color: 'white',
  fontSize: 23,
  fontFamily: 'mikadoultra'
},
center: {
    textAlign: 'center'
},
row: {
    flexDirection: 'row'
},
selectCatText: {
  color: '#1CD9FF',
  fontFamily: 'mikadoultra',
  fontSize: 23
},
fewerText: {
  color: 'white',
  fontSize: 18,
  fontFamily: 'mikadoultra'
},
subjectAvatarContainer: {
  borderRightWidth: 1,
  borderColor: '#ABADAD',
  flex: 1,
  padding: 15
},
subjectListWrapper: {
  justifyContent: 'center',
  borderBottomWidth: 1,
  borderColor: '#ABADAD',
  flex: 1
},
subjectText: {
  fontSize: 16,
  color: '#FF920D',
  fontFamily: 'mikadoultra'
},
avatarStyle: {
  resizeMode: 'contain',
  flex: 1,
  height: undefined,
  width: undefined
},
});

export default connect(null, { addSubjectForQuestion })(SuggestQuestion);
