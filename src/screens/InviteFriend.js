import React, { Component } from 'react';
import {
  View,
  Text,
  ToastAndroid,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
// import Button from 'react-native-button';
import Autocomplete from 'react-native-autocomplete-input';

export default class InviteFriend extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      suggestions: [],
      username: '',
      avatar: ''
    };
  }

  // componentDidMount() {
  //   Sockets.on('online-suggestions', (data) => {
  //     let index = data[0].friends.indexOf(this.state.username);
  //     if(index >= 0){
  //       data[0].friends.splice(index, 1);
  //         this.setState({
  //             suggestions: data[0].friends
  //         });
  //     } else {
  //         this.setState({
  //             suggestions: data[0].friends
  //         });
  //     }
  //   });

    // Sockets.on('invite-result', (data) => {
    //   if (data[0].result) {
    //     this.props.doReplace('challenge');
    //   } else {
    //     ToastAndroid.show('This username does not exist', ToastAndroid.SHORT);
    //   }
    // });

  //   AsyncStorage.getItem('user').then((resp) => {
  //     let user = JSON.parse(resp).user;
  //     this.setState({
  //         username: user.username,
  //         avatar: user.image
  //     });
  //   });
  // }

  // componentWillUnmount() {
  //   NativeModules.SocketIO.off('invite-result');
  //   NativeModules.SocketIO.off('online-suggestions');
  // }

  // getSuggestions(data) {
  //   this.setState({ query: data });
  //   Sockets.emit('online-suggest', { username: data });
  // }

  selectUsername(username) {
    this.setState({ query: username });
    this.setState({
      suggestions: []
    });
  }
  //
  // sendInvite() {
  //   if (this.state.query.length === 0) {
  //     ToastAndroid.show("Please fill in your friend's username", ToastAndroid.SHORT);
  //   } else {
  //     if(this.state.query === this.state.username){
  //       ToastAndroid.show('You cannot challenge yourself', ToastAndroid.SHORT);
  //     } else {
  //       Sockets.emit('send-invite', {username: this.state.query, image: this.state.avatar});
  //     }
  //   }
  // }

  render() {
    const { query } = this.state;
    return (
      <View style={[styles.fullWidth]}>
        <View style={[styles.bodyWidth, styles.backBlack]}>
          <Text style={[styles.center, styles.heading]}>
            INVITE YOUR FRIEND TO A CHALLENGE
          </Text>

          <Autocomplete
              inputContainerStyle={[
                {
                  margin: 10,
                  marginBottom: 0,
                  zIndex: 10 }]}
                  data={this.state.suggestions}
                  defaultValue={query}
                  // onChangeText={text => this.getSuggestions(text)}
                  renderItem={data => (
              <TouchableOpacity
                onPress={() => this.selectUsername(data)}
                style={{ zIndex: 10, paddingVertical: 5 }}
              >
                <Text style={{ color: 'black', fontSize: 16 }}>data</Text>
              </TouchableOpacity>
              )}
          />

          <Text
            style={[styles.center, styles.textStyle,
            { marginTop: 10, marginHorizontal: 30, flex: 1 }]}
          >
            Type the name of your friend below who is also registered to
            WIZI QUIZ and let the games begin...
          </Text>

          <View style={[styles.row, { justifyContent: 'center', marginLeft: 30, marginRight: 30 }]}>
            {/*  <Button
              containerStyle={[{
                padding: 2,
                width: 230,
                borderRadius: 5,
                backgroundColor: '#1EBBA7',
                marginTop: 10
              }]}
              onPress={() => this.sendInvite()}
              style={[{ fontSize: 18, color: 'white' }]}
            >CONTINUE</Button> */}
            <View
              style={[styles.buttonStyle, { backgroundColor: '#1EBBA7' }]}
            >
              <TouchableOpacity>
                <Text style={styles.buttonText}>CONTINUE</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 2 }}>
            <Text style={[styles.center, { color: 'white', marginTop: 10 }]}>
              Challenge mode allows you to play against several challengers
            </Text>
            <Text style={[styles.center, styles.textStyle, { marginTop: 10, marginHorizontal: 30 }]}>
              If you dont see your friends name automatically showing
              up that means they are offline.
             Give them a buzz to request them to come online.
            </Text>

          </View>

          <View style={[styles.row, { justifyContent: 'center', marginTop: 20, marginBottom: 30 }]}>
            {/*<Button
              containerStyle={[{
                marginTop: 20,
                padding: 2,
                width: 230,
                borderRadius: 5,
                backgroundColor: '#FD9D00',
              }]}
              style={[{ fontSize: 18, color: 'white' }]}
            >SEND NOTIFICATION</Button>*/}
            <View style={[styles.buttonStyle, { backgroundColor: '#FD9D00' }]}>
              <TouchableOpacity>
                <Text style={styles.buttonText}>SEND NOTIFICATION</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    fullWidth: {
      flex: 1,
      backgroundColor: '#fff'
    },
    white: {
      color: 'white'
    },

    bodyWidth: {
      flex: 18
    },
    backBlack: {
      backgroundColor: '#000001'
    },
    whiteBack: {
      backgroundColor: '#FEFFFF'
    },
    row: {
      flexDirection: 'row'
    },
    heading: {
      fontSize: 25,
      fontWeight: '900',
      color: '#FD9D00',
      marginTop: 50,
      flex: 2
    },
    center: {
      textAlign: 'center'
    },
    textStyle: {
      fontSize: 12, color: '#fff',
    },
    buttonStyle: {
      padding: 2,
      width: 230,
      borderRadius: 5,
      marginTop: 10,
      justifyContent: 'center',
      alignItems: 'center'
    },
    buttonText: {
      fontSize: 18,
      color: 'white',
      fontWeight: 'bold'
    }
});
