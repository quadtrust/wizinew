import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Header from '../components/Header';
import { getResult } from '../actions/';

 class ProgressReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maths: '0%',
            arabic: '0%',
            islamic: '0%',
            english: '0%',
            social: '0%',
            science: '0%',
            maths_answered: '0',
            arabic_answered: '0',
            islamic_answered: '0',
            english_answered: '0',
            social_answered: '0',
            science_answered: '0',
            work_subject: ''
        };
    }

    componentWillMount() {
      this.props.getResult(this.props.id);
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
              {/*   <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} />
                 <PlayerStats count={this.props.spins} life={this.props.lives} /> */}
                 <Header title={'RESULTS'} />
                 <ScrollView>
                <View style={[styles.bodyWidth]}>

                        <View style={[styles.headerContainer]}>
                            <Text style={[styles.header]}>PROGRESS REPORT</Text>
                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text
                                 style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'center'
                                }}
                                >MATHS</Text>
                                <View
                                 style={{ flexDirection: 'row' }}
                                >
                                  <Image
                                    style={[styles.characterImage]}
                                    source={require('../assets/images/common/maths-character.png')}
                                  />
                                    <Text style={[styles.percentageText]}> {this.props.results.maths !== undefined ? this.props.results.maths.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                 style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                > Questions answered: {this.props.results.maths !== undefined ? this.props.results.maths.all_questions : '0' }</Text>
                            </View>
                            <View
                             style={{ padding: 5,
                                flex: 1,
                                borderLeftWidth: 1,
                                 borderColor: '#BABCBC' }}
                            >
                                <Text
                                  style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'right',
                                    paddingRight: 30
                                  }}
                                >ARABIC</Text>
                                <View
                                style={{ flexDirection: 'row' }}
                                >
                                  <Image
                                    style={[styles.characterImage]}
                                    source={require('../assets/images/common/arabic-character.png')}
                                  />
                                  <Text style={[styles.percentageText]}> {this.props.results.maths !== undefined ? this.props.results.arabic.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                > Questions answered: {this.props.results.maths !== undefined ? this.props.results.arabic.all_questions : '0' }</Text>
                            </View>

                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text
                                    style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'center'
                                }}
                                >ISLAMIC </Text>
                                <View style={{ flexDirection: 'row' }}>
                                  <Image
                                    style={[styles.characterImage]}
                                    source={
                                      require('../assets/images/common/islamic-character.png')}
                                  />
                                  <Text style={[styles.percentageText]}> {this.props.results.maths !== undefined ? this.props.results.islamic_study.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                 style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                >Questions answered: {this.props.results.maths !== undefined ? this.props.results.islamic_study.all_questions : '0' }</Text>
                            </View>
                            <View
                             style={{ padding: 5,
                                flex: 1,
                             borderLeftWidth: 1,
                              borderColor: '#BABCBC' }}
                            >
                                <Text
                                style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'center',
                                }}
                                >ENGLISH</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={
                                          require('../assets/images/common/english-character.png')}
                                    />
                                  <Text style={[styles.percentageText]}> {this.props.results.maths !== undefined ? this.props.results.english.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                >Questions answered: {this.props.results.maths !== undefined ? this.props.results.english.all_questions : '0' }</Text>
                            </View>
                        </View>
                        <View style={[styles.reportContainer]}>
                            <View style={{ flex: 1, padding: 5 }}>
                                <Text
                                style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'center',
                                }}
                                >SOCIAL STUDY</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={
                                          require('../assets/images/common/social-character.png')}
                                    />
                                  <Text style={[styles.percentageText]}> {this.props.results.maths !== undefined ? this.props.results.social_study.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                 style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                >Questions answered: {this.props.results.maths !== undefined ? this.props.results.social_study.all_questions : '0' }</Text>
                            </View>
                            <View
                             style={{ padding: 5,
                                flex: 1,
                                 borderLeftWidth: 1,
                                  borderColor: '#BABCBC' }}
                            >
                                <Text
                                style={{
                                    fontSize: 18,
                                    color: '#F0A23D',
                                    fontFamily: 'mikadoultra',
                                    textAlign: 'right',
                                    paddingRight: 30
                                }}
                                >SCIENCE</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image
                                        style={[styles.characterImage]}
                                        source={
                                          require('../assets/images/common/science-character.png')}
                                    />
                                    <Text
                                     style={[styles.percentageText]}
                                    > {this.props.results.maths !== undefined ? this.props.results.science.percentage_marks : '0' }%</Text>
                                </View>
                                <Text
                                 style={{ fontSize: 10, color: 'white', fontWeight: '800' }}
                                >Questions answered: {this.props.results.maths !== undefined ? this.props.results.science.all_questions : '0' }</Text>
                            </View>


                        </View>
                        <View style={[styles.resultContainer]}>
                            <Text style={[styles.result]}>
                                WIZI RESULTS OVERVIEW
                            </Text>
                        </View>
                        <View style={[styles.tipContainer]}>
                            <Text style={[styles.tip]}>
                                YOU NEED TO WORK ON {this.state.work_subject}
                            </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginLeft: 10,
                            marginRight: 10,
                            paddingTop: 5,
                            paddingBottom: 10
                        }}
                        >
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity>
                                    <Text
                                     style={styles.socialText}
                                    >SPIN AGAIN</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }]}
                                        source={require('../assets/images/common/spin.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity>
                                    <Text
                                     style={styles.socialText}
                                    >DETAILED REPORT</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/detailed.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('HaveQuestion')}
                                >
                                    <Text
                                     style={styles.socialText}
                                    >ADD QUESTIOS</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/question.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Social')}>
                                    <Text
                                     style={styles.socialText}
                                    >INVITE FRIEND</Text>
                                    <Image
                                        style={[{
                                            height: 55,
                                            width: 55,
                                            resizeMode: 'contain',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 20
                                        }]}
                                        source={require('../assets/images/report/invite.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View
                          style={styles.socialContainer}
                        >

                            <View>
                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Icon name="facebook" size={30} color='#fff' style={[styles.iconStyle]} />
                                    <Text
                                     style={styles.socialText}
                                    > SHARE ON FACEBOOK</Text>

                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity
                                   style={{ alignItems: 'center', justifyContent: 'center' }}
                                >
                                <Icon name="twitter" size={30} color="#fff" style={[styles.iconStyle]} />
                                    <Text
                                     style={styles.socialText}
                                    >TWEET</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity
                                   style={{ alignItems: 'center', justifyContent: 'center' }}
                                >
                                  <Icon name="envelope" size={30} color="#fff" style={[styles.iconStyle]} />
                                    <Text
                                     style={styles.socialText}
                                    >SEND VIA MAIL</Text>

                                </TouchableOpacity>
                            </View>
                        </View>

                </View>
                  </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#2B292A'
    },
    // bodyWidth: {
    //     flex: 18
    // },
    tipContainer: { flexDirection: 'row',
     alignItems: 'center',
      justifyContent: 'center',
       backgroundColor: '#4175BB' },
    headerContainer: { flexDirection: 'row',
     alignItems: 'center',
      justifyContent: 'center',
       backgroundColor: '#3FC1FF' },
    resultContainer: { flexDirection: 'row',
     alignItems: 'center',
      justifyContent: 'center',
       backgroundColor: '#63B9A8' },
    header: {
      fontSize: 25,
      color: '#fff',
      fontFamily: 'mikadoultra'
     },
    tip: {
      fontSize: 19,
      color: '#fff',
      fontFamily: 'mikadoultra',
      paddingTop: 10,
      paddingBottom: 10
    },
    result: {
      fontSize: 18,
      color: '#fff',
      fontFamily: 'mikadoultra'
    },
    reportContainer: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderColor: '#BABCBC',
        paddingBottom: 5,
        marginRight: 5,
        marginLeft: 5
    },
    characterImage: {
        resizeMode: 'contain',
        height: 60,
        width: undefined,
        flex: 1
    },
    percentageText: {
        fontSize: 25,
        color: '#63B9A8',
        fontFamily: 'mikadoultra',
        flex: 2,
        textAlign: 'center'
    },
    socialContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 15,
      // paddingTop: 5,
      paddingBottom: 5,
      backgroundColor: '#63B9A8'
    },
    iconStyle: {
        padding: 5
    },
    socialText: {
       fontFamily: 'mikadoultra',
       color: 'white',
       fontSize: 10
     }
});
const mapStateToProps = (state) => ({
  results: state.gamequestion.results,
  id: state.auth.player.player_id
});
export default connect(mapStateToProps, { getResult })(ProgressReport);
