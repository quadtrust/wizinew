import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
// import Button from 'react-native-button';

export default class Lives extends Component {
    render() {
        return (
          <View style={[styles.fullWidth]}>
           <View
              style={[styles.bodyWidth, styles.whiteBack,
              { alignItems: 'center', justifyContent: 'center' }]}
           >
              <View>
                <Text style={{ fontWeight: '700', fontSize: 23, color: 'black' }}>
                    YOU HAVE RUN OUT OF LIVES
                </Text>
              </View>
               <View style={{ alignItems: 'center' }}>
                 <Image
                  style={styles.heartImage}
                  source={require('../assets/images/recharge/heart.png')}
                 />
               </View>
                <View>
                  {/* <Button
                        containerStyle={[{
                            marginTop: 20,
                            padding: 5,
                            width: 230,
                            borderRadius: 5,
                            backgroundColor: '#1EBBA7',
                        }]}
                        style={[{ fontSize: 18, color: 'white' }]}
                    >KD 1.00 RECHARGE NOW</Button>*/}
                    <View
                      style={{ marginTop: 20,
                      padding: 5,
                      width: 230,
                      borderRadius: 5,
                      backgroundColor: '#1EBBA7',
                      justifyContent: 'center',
                      alignItems: 'center'
                     }}
                    >
                      <TouchableOpacity>
                        <Text style={styles.buttonText}>
                          KD 1.00 RECHARGE NOW
                        </Text>
                      </TouchableOpacity>
                    </View>
                </View>
           </View>
         </View>
        );
    }
}

const styles = StyleSheet.create({
  fullWidth: {
    flex: 1,
    backgroundColor: '#fff'
  },
  bodyWidth: {
    flex: 18
  },
  heartImage: {
    height: 90,
    width: 100,
    resizeMode: 'contain',
    margin: 40
  },
    whiteBack: {
    backgroundColor: '#FEFFFF'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold'
  },
});
