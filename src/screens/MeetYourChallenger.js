import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native';
// import Header from './Header'
import Button from 'react-native-button';
// import Sockets from './Sockets'
// import API from './API'

 class MeetYourChallenge extends Component {
    constructor(props) {
        super(props);
        this.startGame = this.startGame.bind(this);
        this.state = {
            challenger1: require('../assets/human/human1.png'),
            challenger2: require('../assets/human/human2.png'),
            user1: 'username',
            user2: '',
            buttonText: 'Waiting...',
            playerJoined: false,
            user2_id: undefined,
            random: false,
            spin: false
        };
    }
    startGame() {
      console.log('heoo');
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
              {/*  <Header doLoad={this.props.doLoad} doPop={this.props.doPop} back={true} /> */}
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text
                     style={[styles.challenge_header, styles.center, { marginTop: 25 }]}
                    >ARE YOU READY?</Text>
                    <Text
                     style={[styles.challenge_sub_header, styles.center, { marginTop: 25 }]}
                    >MEET YOUR CHALLENGER</Text>
                    <View style={[styles.row, { justifyContent: 'space-around', marginTop: 20 }]}>
                        <View style={[styles.center_element]}>
                            <Image
                             style={[styles.contain, { height: 200, width: 250 }]}
                              source={this.state.challenger1}
                            />
                            <Text style={{ color: 'white' }}>{this.state.user1}</Text>
                        </View>
                        <Text
                         style={{ color: '#FD9D00',
                          fontSize: 28,
                           fontWeight: '900',
                          textAlignVertical: 'center' }}
                        >
                            VS
                    </Text>
                        <View style={[styles.center_element]}>
                            <Image
                             style={[styles.contain, { height: 200, width: 250 }]}
                              source={this.state.challenger2}
                            />
                            <Text style={{ color: 'white' }}>{this.state.user2}</Text>
                        </View>
                    </View>
                    <Button
                        containerStyle={[styles.challengeButton, styles.green, {
                            marginTop: 20,
                            marginLeft: 35,
                            marginRight: 35
                        }]}
                        style={[{ fontSize: 18, color: 'white' }]}
                        disabled={!this.state.playerJoined}
                        onPress={() => this.startGame()}
                    >
                        {this.state.buttonText}
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontWeight: '700',
        fontSize: 23,
        color: '#00AEFF',

    },
    selected_header: {
        fontWeight: '700',
        fontSize: 25,
        color: '#00AEFF',

    },
    challenge_header: {
        fontWeight: '900',
        fontSize: 35,
        color: '#ffffff',

    },
    challenge_sub_header: {
        fontWeight: '100',
        fontSize: 23,
        color: '#ffffff',

    },
    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 4,
        resizeMode: 'stretch',
        marginTop: 20,
        marginHorizontal: 20
    },
    whiteBack: {
        backgroundColor: '#FEFFFF'
    },
    center_element: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    languageHeader: { fontWeight: '700', fontSize: 23, color: 'white', textAlign: 'center' },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    challengeButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackFriends: {
      backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    activeBackRandom: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackRandom: {
        backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inactiveClassic: {
        fontSize: 18, color: 'white'
    },
    activeClassic: {
        fontSize: 18,
        color: 'white',
        borderColor: '#F11F43',
        borderBottomWidth: 4
    },
    inactiveChallenge: {
       fontSize: 18, color: 'white'
    },

    activeChallenge: {
        fontSize: 18,
         color: 'white',
         borderColor: '#00C0FF',
          borderBottomWidth: 4
    },

    challengeActive: {
        marginTop: 20,
        height: 35,
        borderRadius: 5,
        backgroundColor: '#F11F43',
        width: 150
    },
    classicActive: {
        marginTop: 20,
        height: 35,
        borderRadius: 5,
        backgroundColor: '#00C0FF',
        width: 150
    },
    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },
    competitionText: {
        color: '#139BFF',
        fontWeight: '900',
        fontSize: 30,

    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        marginTop: 10,
        position: 'absolute',
        zIndex: 1000
    },
    nextText: {
        fontWeight: '800', fontSize: 20, color: '#fff'
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15,
        paddingTop: 2
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        paddingHorizontal: 35,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },

    textCenter: {
        textAlign: 'center'
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 30,
        textDecorationLine: 'underline',

    },

});
export default MeetYourChallenge;
