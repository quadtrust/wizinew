import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import Header from '../components/Header';

class CharacterUnlocked extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: null,
            user: ''
        };
    }

    // componentDidMount() {
    //     AsyncStorage.getItem('user').then((user) => {
    //         user = JSON.parse(user);
    //         this.setState({
    //             user: user.user.username.toUpperCase()
    //         });
    //     });
    //     AsyncStorage.getItem('character').then((char) => {
    //         API.update_avatar(char, this.props.doResetTo, (response) => {});
    //         switch (JSON.parse(char)) {
    //             case 1:
    //                 this.setState({avatar: require('../assets/human/human1.png')});
    //                 break;
    //             case 2:
    //                 this.setState({avatar: require('../assets/monster/monster1.png')});
    //                 break;
    //             case 3:
    //                 this.setState({avatar: require('../assets/human/human2.png')});
    //                 break;
    //             case 4:
    //                 this.setState({avatar: require('../assets/monster/monster2.png')});
    //                 break;
    //             case 5:
    //                 this.setState({avatar: require('../assets/human/human3.png')});
    //                 break;
    //             case 6:
    //                 this.setState({avatar: require('../assets/monster/monster3.png')});
    //                 break;
    //             case 7:
    //                 this.setState({avatar: require('../assets/human/human4.png')});
    //                 break;
    //             case 8:
    //                 this.setState({avatar: require('../assets/monster/monster4.png')});
    //                 break;
    //             case 9:
    //                 this.setState({avatar: require('../assets/human/human5.png')});
    //                 break;
    //             case 10:
    //                 this.setState({avatar: require('../assets/monster/monster5.png')});
    //                 break;
    //             case 11 :
    //                 this.setState({avatar: require('../assets/human/human6.png')});
    //                 break;
    //             case 12:
    //                 this.setState({avatar: require('../assets/monster/monster6.png')});
    //                 break;
    //             case 13 :
    //                 this.setState({avatar: require('../assets/human/human7.png')});
    //                 break;
    //             case 14:
    //                 this.setState({avatar: require('../assets/monster/monster7.png')});
    //                 break;
    //             case 15:
    //                 this.setState({avatar: require('../assets/human/human8.png')});
    //                 break;
    //             case 16 :
    //                 this.setState({avatar: require('../assets/monster/monster8.png')});
    //                 break;
    //             case  17:
    //                 this.setState({avatar: require('../assets/human/human9.png')});
    //                 break;
    //             case 18:
    //                 this.setState({avatar: require('../assets/monster/monster9.png')});
    //                 break;
    //             case 19 :
    //                 this.setState({avatar: require('../assets/human/human10.png')});
    //                 break;
    //             case 20:
    //                 this.setState({avatar: require('../assets/monster/monster10.png')});
    //                 break;
    //             case 21:
    //                 this.setState({avatar: require('../assets/human/human11.png')});
    //                 break;
    //             case 22:
    //                 this.setState({avatar: require('../assets/monster/monster11.png')});
    //                 break;
    //             case 23 :
    //                 this.setState({avatar: require('../assets/human/human12.png')});
    //                 break;
    //             case 24:
    //                 this.setState({avatar: require('../assets/monster/monster12.png')});
    //                 break;
    //             case 25:
    //                 this.setState({avatar: require('../assets/monster/monster13.png')});
    //                 break;
    //             case 26:
    //                 this.setState({avatar: require('../assets/monster/monster14.png')});
    //                 break;
    //             case 27:
    //                 this.setState({avatar: require('../assets/monster/monster15.png')});
    //                 break;
    //             case 28:
    //                 this.setState({avatar: require('../assets/monster/monster16.png')});
    //                 break;
    //             case 29:
    //                 this.setState({avatar: require('../assets/monster/monster17.png')});
    //                 break;
    //             case 30:
    //                 this.setState({avatar: require('../assets/monster/monster18.png')});
    //                 break;
    //             case 31:
    //                 this.setState({avatar: require('../assets/monster/monster19.png')});
    //                 break;
    //             case 32:
    //                 this.setState({avatar: require('../assets/monster/monster20.png')});
    //                 break;
    //             case 33:
    //                 this.setState({avatar: require('../assets/monster/monster21.png')});
    //                 break;
    //             case 34:
    //                 this.setState({avatar: require('../assets/monster/monster22.png')});
    //                 break;
    //             case 35:
    //                 this.setState({avatar: require('../assets/monster/monster23.png')});
    //                 break;
    //             case 36:
    //                 this.setState({avatar: require('../assets/monster/monster24.png')});
    //                 break;
    //             case 37:
    //                 this.setState({avatar: require('../assets/monster/monster25.png')});
    //                 break;
    //             case 38:
    //                 this.setState({avatar: require('../assets/monster/monster26.png')});
    //                 break;
    //             case 39:
    //                 this.setState({avatar: require('../assets/monster/monster27.png')});
    //                 break;
    //             case 40:
    //                 this.setState({avatar: require('../assets/monster/monster28.png')});
    //                 break;
    //             case 41:
    //                 this.setState({avatar: require('../assets/monster/monster29.png')});
    //                 break;
    //             case 42:
    //                 this.setState({avatar: require('../assets/monster/monster30.png')});
    //                 break;
    //             case 43:
    //                 this.setState({avatar: require('../assets/monster/monster31.png')});
    //                 break;
    //             case 44:
    //                 this.setState({avatar: require('../assets/monster/monster32.png')});
    //                 break;
    //             case 45:
    //                 this.setState({avatar: require('../assets/monster/monster33.png')});
    //                 break;
    //             case 46:
    //                 this.setState({avatar: require('../assets/monster/monster34.png')});
    //                 break;
    //             case 47:
    //                 this.setState({avatar: require('../assets/monster/monster35.png')});
    //                 break;
    //             case 48:
    //                 this.setState({avatar: require('../assets/monster/monster36.png')});
    //                 break;
    //             case 49:
    //                 this.setState({avatar: require('../assets/monster/monster37.png')});
    //                 break;
    //             case 50:
    //                 this.setState({avatar: require('../assets/monster/monster38.png')});
    //                 break;
    //
    //             default:
    //                 this.setState({avatar: require('../assets/monster/monster1.png')});
    //         }
    //     })
    // }

    componentDidMount() {
      console.log('congrats', this.props.lastCharacter);
      switch (this.props.lastCharacter) {
        case 1: {
          this.setState({ avatar: require('../assets/human/human1.png') });

          break;
        }
        case 2: {
          this.setState({ avatar: require('../assets/human/human2.png') });

          break;
        }

        case 3: {
          this.setState({ avatar: require('../assets/human/human3.png') });

          break;
        }

        case 4: {
          this.setState({ avatar: require('../assets/human/human4.png') });

          break;
        }
        case 5: {
          this.setState({ avatar: require('../assets/human/human5.png') });

          break;
        }
        case 6: {
          this.setState({ avatar: require('../assets/human/human6.png') });

          break;
        }
        case 7: {
          this.setState({ avatar: require('../assets/human/human7.png') });

          break;
        }
        case 8: {
          this.setState({ avatar: require('../assets/human/human8.png') });

          break;
        }
        case 9: {
          this.setState({ avatar: require('../assets/human/human9.png') });

          break;
        }
        case 10: {
          this.setState({ avatar: require('../assets/human/human10.png') });

          break;
        }
        case 11: {
          this.setState({ avatar: require('../assets/human/human11.png') });

          break;
        }
        case 12: {
          this.setState({ avatar: require('../assets/human/human12.png') });

          break;
        }

      }
    }

    render() {
        return (
            <View style={[styles.fullWidth]}>
              <Header title={'WIZI QUIZ'} />
                <View style={[styles.bodyWidth, styles.backBlack]}>
                    <Text
                    style={[styles.selected_header, styles.center, { marginTop: 10 }]}
                    >CONGRATULATIONS!</Text>
                    <Text style={[styles.selected_header, styles.center, { paddingHorizontal: 7, color: 'orange' }]}>You have unlocked the character</Text>
                    <View
                    style={[styles.row, styles.center_element]}
                    >
                        <Image
                        style={[styles.contain, { height: 380, width: 350 }]}
                               source={this.state.avatar}
                        />
                    </View>
                    <Text style={[styles.center, { color: 'white', fontSize: 20 }]}>
                        {this.state.user}
                    </Text>
                    <TouchableOpacity
                    style={{ marginTop: 5 }}
                        onPress={() => this.props.navigation.navigate('Results')}
                    >
                        <Text style={[styles.center, { color: 'white', fontSize: 25 }]}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    fullWidth: {
        flex: 1,
        backgroundColor: '#fff'
    },
    white: {
        color: 'white'
    },
    justify_space_between: {
        justifyContent: 'space-between'
    },
    justify_center: {
        justifyContent: 'center'
    },
    orange_circle: {
        marginTop: 50, backgroundColor: '#EF6C50', width: 8, height: 8, borderRadius: 50
    },
    avatar_header: {
        fontWeight: '700',
        fontSize: 23,
        color: '#00AEFF',

    },
    selected_header: {
        fontFamily: 'mikadoultra',
        fontSize: 25,
        color: '#00AEFF',

    },
    challenge_header: {
        fontWeight: '900',
        fontSize: 35,
        color: '#ffffff',

    },
    challenge_sub_header: {
        fontWeight: '100',
        fontSize: 23,
        color: '#ffffff',

    },
    avatar_size: {
        height: 120,
        width: 75,
        resizeMode: 'contain'
    },
    bodyWidth: {
        flex: 18
    },
    backBlack: {
        backgroundColor: '#000001'
    },
    blackBoard: {
        height: null,
        width: null,
        flex: 4,
        resizeMode: 'stretch',
        marginTop: 20,
        marginHorizontal: 20
    },
    whiteBack: {
        backgroundColor: '#FEFFFF'
    },
    center_element: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row'
    },
    zoomed: {
        resizeMode: 'cover'
    },
    languageHeader: { fontWeight: '700', fontSize: 23, color: 'white', textAlign: 'center' },
    notKnown: {
        height: undefined,
        width: undefined
    },
    contain: {
        resizeMode: 'contain'
    },
    socialButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    challengeButton: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 10
    },
    margins: {
        marginLeft: 50,
        marginRight: 50
    },
    headline: {
        fontSize: 25,
        marginLeft: 20,
        marginRight: 20,
        color: 'orange',
        fontWeight: '800'
    },
    red: {
        backgroundColor: '#F44E4B'
    },
    activeBackFriends: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackFriends: {
      backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    activeBackRandom: {
        backgroundColor: 'grey',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inActiveBackRandom: {
        backgroundColor: '#FD6F0D',
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        width: 150
    },
    inactiveClassic: {
        fontSize: 18, color: 'white'
    },
    activeClassic: {
        fontSize: 18, color: 'white', borderColor: '#F11F43', borderBottomWidth: 4
    },

    activeChallenge: {
        fontSize: 18, color: 'white', borderColor: '#00C0FF', borderBottomWidth: 4
    },

    challengeActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#F11F43',
        width: 150
    },
    classicActive: {
        marginTop: 20,
        height: 27,
        borderRadius: 5,
        backgroundColor: '#00C0FF',
        width: 150
    },
    green: {
        backgroundColor: '#1EBBA7'
    },

    black: {
        color: 'black'
    },
    smallFont: {
        fontSize: 12
    },
    blackColor: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 12
    },
    center: {
        textAlign: 'center'
    },
    field: {
        height: 35,
        backgroundColor: '#EFE9E9',
        padding: 5
    },
    competitionText: {
        color: '#139BFF',
        fontWeight: '900',
        fontSize: 30,

    },
    teacherLogo: {
        resizeMode: 'contain',
        height: 120,
        width: 130,
        marginTop: 10,
        position: 'absolute',
        zIndex: 1000
    },
    nextText: {
        fontWeight: '800', fontSize: 20, color: '#fff'
    },
    choice: {
        textAlign: 'center',
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15,
        paddingTop: 2
    },
    choiceBackground: {
        height: undefined,
        width: undefined,
        flex: 1,
        resizeMode: 'contain',
        paddingHorizontal: 35,
        paddingVertical: 5,
        justifyContent: 'center'
    },
    flexContainer: {
        flex: 1,
        marginLeft: 50,
        marginRight: 50
    },
    activeBack: {
        backgroundColor: 'grey',
        borderRadius: 20
    },
    notActive: {
        backgroundColor: 'transparent'
    },
    heading: {
        fontSize: 25,
        fontWeight: '800',
        color: 'orange'
    },
    subIcon: { height: 130, width: 130, margin: 8 },

    textCenter: {
        textAlign: 'center'
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 30,
        textDecorationLine: 'underline',

    },

});

const mapStateToProps = (state) => ({
  lastCharacter: state.gamequestion.currentUnlock,
});
export default connect(mapStateToProps, {})(CharacterUnlocked);
