import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ProgressBarAndroid,
  NativeModules
} from 'react-native';
// import Header from './Header';
// import PlayerStats from './PlayerStats';
// import API from './API';
// import Language from './Language';
// import Sockets from './Sockets';
import DialogBox from 'react-native-dialogbox';

let { height, width } = Dimensions.get('screen');

let intervalHandler, newCounter = 0;

export default class SubjectVersus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      livesLeft: 10,
      progress: 0,
      questions: false,
      i: 0,
      answered: false,
      button1: require('../assets/images/common/1.png'),
      button2: require('../assets/images/common/1.png'),
      button3: require('../assets/images/common/1.png'),
      button4: require('../assets/images/common/1.png'),
      remark1: 'Remark One',
      remark2: 'Remark Two',
      teacher: require('../assets/images/common/monster.png'),
      background: require('../assets/images/islam/islam.png'),
      subject: 'ENGLISH',
      user2: 'Mritunjay',
      unlockCharacter: false
    }
  }

  // componentDidMount() {
  //   if (this.props.data && this.props.data.user2_username) {
  //     this.setState({ user2: this.props.data.user2_username });
  //   }
  //
  //   AsyncStorage.getItem('question').then((resp) => {
  //     this.setState({ questions: JSON.parse(resp) });
  //   });
  //   this.runTimer();
  //
  //   AsyncStorage.getItem('theme').then((resp) => {
  //     switch (resp) {
  //       case 'english':
  //         this.setState({
  //           background: require('../assets/images/english/english.png'),
  //           subject: Language.english,
  //           teacher: require('../assets/images/common/english-character.png')
  //         });
  //         break;
  //
  //       case 'maths':
  //         this.setState({
  //           background: require('../assets/images/maths/maths.png'),
  //           subject: Language.maths,
  //           teacher: require('../assets/images/common/maths-character.png')
  //         });
  //         break;
  //
  //       case 'islam':
  //         this.setState({
  //           background: require('../assets/images/islam/islam.png'),
  //           subject: Language.islamic,
  //           teacher: require('../assets/images/common/islamic-character.png')
  //         });
  //         break;
  //
  //       case 'social':
  //         this.setState({
  //           background: require('../assets/images/sst/sst.png'),
  //           teacher: require('../assets/images/common/social-character.png'),
  //           subject: Language.social
  //         });
  //         break;
  //
  //       case 'science':
  //         this.setState({
  //           background: require('../assets/images/science/science.png'),
  //           teacher: require('../assets/images/common/science-character.png'),
  //           subject: Language.science
  //         });
  //         break;
  //
  //       default:
  //         this.setState({
  //           background: require('../assets/images/arabic/arabic.png'),
  //           teacher: require('../assets/images/common/arabic-character.png'),
  //           subject: Language.arabic
  //         });
  //     }
  //   });
  //
  //   Sockets.on('next-question', () => {
  //     setTimeout(() => {
  //       this.nextQuestion();
  //     }, 1000);
  //   });
  // }

  // componentWillUnmount() {
  //   Sockets.off('next-question');
  //   clearInterval(intervalHandler);
  // }

  // runTimer() {
  //   clearInterval(intervalHandler);
  //   intervalHandler = setInterval(() => {
  //     if (this.state.progress < 1) {
  //       this.setState({
  //         progress: this.state.progress + 0.01
  //       });
  //     } else {
  //       clearInterval(intervalHandler);
  //       this.setState({
  //           answered: true
  //       });
  //       Sockets.emit('answered', { username: this.props.data.user2_username });
  //     }
  //   }, 100);
  // }

  // stopTimer() {
  //   clearInterval(intervalHandler);
  // }

  // nextQuestion() {
  //   if ((this.state.i + 1) > 9) {
  //     this.props.gameOver();
  //     clearInterval(intervalHandler);
  //     Sockets.off('next-question');
  //     if(this.state.unlockCharacter){
  //         this.props.doReplace('unlockCharacter');
  //     } else {
  //         this.props.doReplace('result');
  //     }
  //   } else {
  //     this.setState({
  //       i: this.state.i + 1,
  //       progress: 0,
  //       answered: false
  //     });
  //     this.runTimer();
  //     this.resetChoices();
  //   }
  // }

  // resetChoices() {
  //   this.setState({
  //     button1: require('../assets/images/common/1.png'),
  //     button2: require('../assets/images/common/1.png'),
  //     button3: require('../assets/images/common/1.png'),
  //     button4: require('../assets/images/common/1.png')
  //   });
  // }
  //
  // wrongChoice(choice) {
  //   switch (choice) {
  //     case 1:
  //       this.setState({
  //         button1: require('../assets/images/common/3.png')
  //       });
  //       break;
  //
  //     case 2:
  //       this.setState({
  //         button2: require('../assets/images/common/3.png')
  //       });
  //       break;
  //
  //     case 3:
  //       this.setState({
  //         button3: require('../assets/images/common/3.png')
  //       });
  //       break;
  //
  //     case 4:
  //       this.setState({
  //         button4: require('../assets/images/common/3.png')
  //       });
  //       break
  //   }
  // }
  //
  // correctChoice(choice) {
  //   switch (choice) {
  //     case 1:
  //       this.setState({
  //         button1: require('../assets/images/common/2.png')
  //       });
  //       break;
  //
  //     case 2:
  //       this.setState({
  //         button2: require('../assets/images/common/2.png')
  //       });
  //       break;
  //
  //     case 3:
  //       this.setState({
  //         button3: require('../assets/images/common/2.png')
  //       });
  //       break;
  //
  //     case 4:
  //       this.setState({
  //         button4: require('../assets/images/common/2.png')
  //       });
  //       break
  //   }
  // }

  checkAnswer(choice) {
    console.log(choice);
    // setTimeout(() => {
    //     Sockets.emit('answered', { username: this.props.data.user2_username });
    // }, parseInt(Math.random() * 1000));
    // this.stopTimer();
    // let flag = false;
    // if (this.state.questions.questions[this.state.i].correct_choice == choice) {
    //   flag = true;
    // }
    //
    // if (flag) {
    //   this.correctChoice(choice);
    //   if(this.state.i === 0){
    //       this.setState({
    //           unlockCharacter: true
    //       });
    //   }
    // } else {
    //   this.wrongChoice(choice);
    //   this.correctChoice(parseInt(this.state.questions.questions[this.state.i].correct_choice));
    //   this.setState({
    //       unlockCharacter: false
    //   });
    // }
    //
    // API.nextQuestion(this.state.questions.questions[this.state.i].GameQuestionId, { choice: choice }, this.props.doResetTo, (data) => { });
    //
    // this.setState({
    //   answered: true,
    //   remark1: flag ? Language.excellent : Language.too_bad,
    //   remark2: flag ? Language.correct_answers : Language.wrong_answer
    // });

    // setTimeout(() => {
    //     this.nextQuestion();
    //     this.runTimer();
    // }, 1000);
  }

  // end_game() {
  //     let question_id = this.state.questions ? this.state.questions.questions[this.state.i].GameQuestionId : 0;
  //     this.dialogbox.confirm({
  //         title: 'End Game',
  //         content: 'Do you really want to end game?',
  //         ok: {
  //             text: 'Yes',
  //             callback: () => {
  //                 API.end_game(() => {
  //                 }, question_id, (resp) => {
  //                     if (resp.status) {
  //                         this.props.doResetTo('result');
  //                     } else {
  //                         ToastAndroid.show('Some error occurred', ToastAndroid.SHORT);
  //                     }
  //                 });
  //             }
  //         }
  //     });
  // }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/*  <Header doLoad={this.props.doLoad}
         doPop={this.props.doPop} doResetTo={this.props.doResetTo}
                  endGame={true} end_game={() => this.end_game()} />
          <PlayerStats life={this.props.lives} count="1" /> */}
          <View style={{ flex: 18 }}>
              <ProgressBarAndroid
              color="red" progress={this.state.progress}
               style={{ height: 3 }} styleAttr="Horizontal" indeterminate={false}
              />
              <Image
              style={{ width: undefined, height: undefined, flex: 1, resizeMode: 'cover' }}
               source={this.state.background}
              >
                  <Image
                   style={{ flex: 8,
                      height: undefined,
                       width: undefined,
                        resizeMode: 'contain',
                         margin: 20 }}
                         source={require('../assets/images/common/board.png')}
                  >
                      <Text
                       style={{ fontSize: 25,
                          color: '#fff',
                           fontWeight: '700',
                            textAlign: 'center',
                             marginTop: 20,
                              textDecorationLine: 'underline' }}
                      >{this.state.subject}</Text>
                      <Text
                       style={{ fontSize: 16,
                          color: '#fff',
                           fontWeight: 'bold',
                            textAlign: 'center',
                             marginLeft: 40,
                              marginRight: 40 }}
                      >{this.state.questions ?
                        this.state.questions.questions[this.state.i].question : ''}</Text>
                      <View style={{ flex: 1, marginBottom: 20 }}>
                          <TouchableOpacity
                           disabled={this.state.answered}
                            onPress={() => this.checkAnswer(1)}
                             style={{ flex: 1, marginLeft: 50, marginRight: 50 }}
                          >
                              <Image
                               style={{ height: undefined,
                                  width: undefined,
                                   flex: 1,
                                    resizeMode: 'contain',
                                     alignItems: 'center',
                                      justifyContent: 'center' }}
                                       source={this.state.button1}
                              >
                                  <Text
                                   style={{ color: 'black', fontSize: 16, fontWeight: 'bold' }}
                                  >{this.state.questions ?
                                     this.state.questions.questions[this.state.i].choice1 : ''}
                                  </Text>
                              </Image>
                          </TouchableOpacity>
                          <TouchableOpacity
                           disabled={this.state.answered}
                            onPress={() => this.checkAnswer(2)}
                             style={{ flex: 1, marginLeft: 50, marginRight: 50 }}
                          >
                              <Image
                               style={{ height: undefined,
                                  width: undefined,
                                   flex: 1,
                                    resizeMode: 'contain',
                                     alignItems: 'center',
                                      justifyContent: 'center' }}
                                       source={this.state.button2}
                              >
                                  <Text
                                   style={{ color: 'black',
                                    fontSize: 16,
                                     fontWeight: 'bold' }}
                                  >{this.state.questions ?
                                     this.state.questions.questions[this.state.i].choice2 : ''}
                                  </Text>
                              </Image>
                          </TouchableOpacity>
                          <TouchableOpacity
                           disabled={this.state.answered}
                            onPress={() => this.checkAnswer(3)}
                             style={{ flex: 1, marginLeft: 50, marginRight: 50 }}
                          >
                              <Image
                               style={{ height: undefined,
                                  width: undefined,
                                   flex: 1,
                                    resizeMode: 'contain',
                                     alignItems: 'center',
                                      justifyContent: 'center' }}
                                       source={this.state.button3}
                              >
                                  <Text
                                   style={{ color: 'black',
                                    fontSize: 16,
                                     fontWeight: 'bold' }}
                                  >{this.state.questions ?
                                     this.state.questions.questions[this.state.i].choice3 : ''}
                                  </Text>
                              </Image>
                          </TouchableOpacity>
                          <TouchableOpacity
                           disabled={this.state.answered}
                            onPress={() => this.checkAnswer(4)}
                             style={{ flex: 1, marginLeft: 50, marginRight: 50 }}
                          >
                              <Image
                               style={{ height: undefined,
                                  width: undefined,
                                   flex: 1,
                                    resizeMode: 'contain',
                                     alignItems: 'center',
                                      justifyContent: 'center' }}
                                       source={this.state.button4}
                              >
                                  <Text
                                   style={{ color: 'black',
                                    fontSize: 16,
                                     fontWeight: 'bold' }}
                                  >{this.state.questions ?
                                     this.state.questions.questions[this.state.i].choice4 : ''}
                                  </Text>
                              </Image>
                          </TouchableOpacity>
                      </View>
                  </Image>
                  <Image
                  source={this.state.teacher}
                  style={{ height: 100,
                     width: 100,
                      left: 10,
                       position: 'absolute',
                        resizeMode: 'contain',
                         zIndex: 2,
                          opacity: this.state.answered ? 1 : 0 }}
                  />
                  <View
                   style={{ flex: 2,
                      marginBottom: 5,
                       marginTop: 10,
                        opacity: this.state.answered ? 1 : 0 }}
                  >
                      <View
                       style={{ flex: 3,
                          backgroundColor: 'rgba(0,0,0,0.65)',
                           justifyContent: 'space-between' }}
                      >
                          <View style={{ flex: 1, flexDirection: 'row' }}>
                              <View style={{ flex: 2 }}><Text /></View>
                              <View style={{ flex: 5 }}><Text
                                style={{
                                            fontSize: 24,
                                            color: 'orange',
                                            fontWeight: '800',
                                            textAlign: 'center'
                                        }}
                              >{this.state.remark1}</Text></View>
                          </View>
                          <View style={{ flex: 1, flexDirection: 'row' }}>
                              <View style={{ flex: 2 }}><Text /></View>
                              <View style={{ flex: 5 }}>
                              <Text
                              style={{
                                        fontSize: 24,
                                        color: '#fff',
                                        fontWeight: '800',
                                        textAlign: 'center',
                                        borderColor: 'white',
                                        lineHeight: 21
                                    }}
                              >{this.state.remark2}</Text>
                              </View>
                          </View>
                      </View>
                      <View style={{ flex: 1 }} />
                  </View>
              </Image>
          </View>
          <DialogBox ref={(dialogbox) => { this.dialogbox = dialogbox; }} />
      </View>
    );
  }
}
