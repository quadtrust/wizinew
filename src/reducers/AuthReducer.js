const INITIAL_STATE = {
  user: {},
  profilepic: null,
  player: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'ADD_FB_TOKEN': {
      return {
        ...state,
        user: action.payload
      };
    }
    case 'PLAYER_FETCH': {
      return { ...state, player: action.payload };
    }
    case 'PROFILE_PIC_UPDATE': {
      return { ...state, profilepic: action.payload };
    }
    default:
      return state;
  }
};
