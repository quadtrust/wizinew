import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import RegisterReducer from './RegisterReducer';
import GameQuestionReducers from './GameQuestionsReducers';
import SettingsReducer from './SettingsReducer';
import Question from './QuestionReducer.js';

export default combineReducers({
  auth: AuthReducer,
  register: RegisterReducer,
  gamequestion: GameQuestionReducers,
  settings: SettingsReducer,
  Questions: Question
});
