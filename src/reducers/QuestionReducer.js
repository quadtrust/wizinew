const INITIAL_STATE = {
  subject: '',
  question: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'addSubjectForQuestion': {
      return {
        ...state, subject: action.payload
      };
    }
    case 'submitQuestion': {
      return { ...state, question: action.payload };
    }
    default:
      return state;
  }
};
