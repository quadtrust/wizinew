const INITIAL_STATE = {
  registerdata: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'INPUT_FIELD_UPDATE': {
    const tempRegister = state.registerdata;
      tempRegister[action.payload.prop] = action.payload.value;
      return { ...state, registerdata: tempRegister };
    }
    default:
      return state;
  }
};
