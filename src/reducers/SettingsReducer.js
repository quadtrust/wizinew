const INITIAL_STATE = {
	music: true,
	vibration: true
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'MUSIC_SETTING':
			return { ...state, music: action.payload };
		case 'VIBRATION_SETTING':
			return { ...state, vibration: action.payload };
		default:
			return state;
	}
};
