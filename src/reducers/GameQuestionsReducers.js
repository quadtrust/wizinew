const INITIAL_STATE = {
  questions: [],
  subject: '',
  spinsCount: 10,
  unlockCharacterList: [],
  currentUnlock: null,
  results: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'QUESTION_FETCHED': {
      return { ...state, questions: action.payload };
    }
    case 'SUBJECT_UPLOAD': {
      return { ...state, subject: action.payload };
    }
    case 'UNLOCK_CHARACTER': {
      return { ...state, currentUnlock: action.payload };
    }
    case 'FETCH_UNLOCK_CHARACTERS': {
      const objArr = action.payload;
      const idArr = [];
      for (let i = 0; i < objArr.length; i++) {
        idArr.push(objArr[i].character);
      }
      return { ...state, unlockCharacterList: idArr };
    }
    case 'REDUCE_SPINS': {
      return { ...state, spinsCount: action.payload };
    }
    case 'RESULT_FETCH': {
      return { ...state, results: action.payload };
    }
    default:
      return state;
  }
};
