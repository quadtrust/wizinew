// import React, { Component } from 'react';
// import {
//     AsyncStorage
// } from 'react-native';
import Sound from 'react-native-sound';


let back,
  background,
  gameOver,
  rightAnswer,
  spinner,
  wrongAnswer,
  backgroundMusic = false,
  initialized = false;

class Sounds {

    static loadSounds() {
        if (!initialized) {
            back = new Sound('back.wav', Sound.MAIN_BUNDLE);
            background = new Sound('background.mp3', Sound.MAIN_BUNDLE);
            gameOver = new Sound('game_over.wav', Sound.MAIN_BUNDLE);
            rightAnswer = new Sound('right_asnwer.wav', Sound.MAIN_BUNDLE);
            spinner = new Sound('spinner.wav', Sound.MAIN_BUNDLE);
            wrongAnswer = new Sound('wrong_answer', Sound.MAIN_BUNDLE);
        }
    }

    static playBackground() {
        background.stop();
        background.setNumberOfLoops(-1);
        background.play();
        backgroundMusic = true;
    }

    static stopBackground() {
      console.log('stop');
        background.pause();
        background.stop();
        backgroundMusic = false;
    }

    static playBack() {
        back.play();
        backgroundMusic = true;
    }

    static playGameOver() {
        gameOver.play();
    }

    static playRightAnswer() {
        rightAnswer.play();
    }

    static playSpinner() {
        spinner.play();
    }

    static playWrongAnswer() {
        wrongAnswer.play();
    }

}


export default Sounds;
