/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

import App from './src/App';

import { Sentry } from 'react-native-sentry';

Sentry.config("https://98fb815ef002401fa58c7234d71416b1:36c41bd7a14e4ad599606ffd69931f88@sentry.io/183243").install();


export default class Wizi extends Component {
  render() {
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('Wizi', () => Wizi);
